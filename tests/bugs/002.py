# If a file or a directory is deleted between the time user is asked about it,
# and the time rsync command line is built, an exception will arise and the
# program will abort.
# 
# What should happen is: display a warning message about the deleted file, and
# continue.
# 
# While correcting this bug, also consider the case where this file has been
# turned into a directory, or the converse way. In clear, look for any change
# in information about files.

import pexpect
import sys
import os
homedir = os.getcwd()
env = {"HOME": homedir, "SHELL": "sh" }
srsync = os.path.abspath("../../srsync") + " "

os.mkdir("dir")

child = pexpect.spawn(srsync + 'print . --one-shot --shell-output stdout', env = env)
child.logfile = sys.stdout;

child.expect('Choose action to perform on "."')
child.sendline('d')

i = 0
while i != 2:
    i = child.expect(['Choose action to perform on "./dir', 'Choose action to perform on', pexpect.EOF])
    if (i == 1):
        child.sendline('n')
    if (i == 0):
        child.sendline('$')
        child.sendline('rmdir ../dir')
        child.sendline('exit')
        child.sendline('y')

sys.exit(0)
