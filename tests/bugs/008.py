import pexpect
import sys
import os
homedir = os.getcwd()
env = {"HOME": homedir, "SHELL": "sh", "PAGER": "cat"}
srsync = os.path.abspath("../../srsync") + " "

DIRNAME = "dir with spaces"
FILENAME = "file with spaces\"; touch \"dangerous"
os.mkdir(DIRNAME)
open(FILENAME, 'w').close()


child = pexpect.spawn(srsync + 'print none . --one-shot --shell-output stdout', env = env)
child.logfile = sys.stdout;

child.expect('Choose action to perform on "."')
child.sendline('d')

i = 0
fail = False
while i != 3:
    i = child.expect([
        'Choose action to perform on ".*with spaces"',
        'Choose action to perform on',
        'No such file or directory',
        pexpect.EOF])
    if (i == 0):
        child.sendline('l')
        child.sendline('n')
    if (i == 1):
        child.sendline('n')
    if (i == 2):
        fail = True

os.rmdir(DIRNAME)
os.remove(FILENAME)

if fail:
    sys.exit(1)
else:
    sys.exit(0)
