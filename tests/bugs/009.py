import pexpect
import sys
import os
homedir = os.getcwd()
env = {"HOME": homedir, "SHELL": "sh", "PAGER": "cat"}
srsync = os.path.abspath("../../srsync") + " "

FILENAME = "file with spaces\"; touch \"dangerous"
open(FILENAME, 'w').close()


child = pexpect.spawn(srsync + 'print none . --one-shot --shell-output stdout', env = env)
child.logfile = sys.stdout;

child.expect('Choose action to perform on "."')
child.sendline('d')

i = 0
while i != 2:
    i = child.expect([
        'Choose action to perform on ".*with spaces"',
        'Choose action to perform on',
        pexpect.EOF])
    if (i == 0):
        child.sendline('l')
        child.sendline('n')
    if (i == 1):
        child.sendline('n')

os.remove(FILENAME)

if (os.path.exists('dangerous')):
    os.remove('dangerous')
    sys.exit(1)
else:
    sys.exit(0)
