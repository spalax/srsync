import pexpect
import sys
import os
homedir = os.getcwd()
env = {"HOME": homedir, "SHELL": "sh", "PAGER": "cat"}
srsync = os.path.abspath("../../srsync") + " "

child = pexpect.spawn(srsync + 'print none . --one-shot --shell-output stdout', env = env)
child.logfile = sys.stdout;

child.expect('Choose action to perform on "."')
child.sendline('a')
child.expect('Choose action to perform on "."')
child.sendline('a')
child.expect('Choose action to perform on "."')
child.sendline('h')

i = child.expect([
    'a: always ask for action',
    'a: stop asking',
    ])
print ""
if (i==0):
    sys.exit(0)
else:
    sys.exit(1)
