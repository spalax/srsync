import pexpect
import sys
import os

import action_remove_common

homedir = os.getcwd()
env = {"HOME": homedir, "SHELL": "sh", "PAGER": "cat"}
srsync = os.path.abspath("../../srsync") + " "

action_remove_common.rm_fr("src")
action_remove_common.rm_fr("dest")
action_remove_common.create_dummy_tree({"src" : {"dir1" : {'': ['1', '2'], 'dir':{}}, "dir2" : {}, "dir3" : {'': ["1", "2"]}}, "dest":{}})

child = pexpect.spawn(srsync + 'exec print --one-shot --shell-output=stdout src dest', env = env)
child.logfile = sys.stdout;

child.expect('Choose action to perform on "src"')
child.sendline('d')
child.expect('Choose action to perform on "src/dir1"')
child.sendline('r')
child.expect('Are you sure you want to remove "src/dir1"')
child.sendline('y')
child.expect('Choose action to perform on "src/dir2"')
child.sendline('r')
child.expect('Are you sure you want to remove "src/dir2"')
child.sendline('n')
child.expect('Choose action to perform on "src/dir2"')
child.sendline('s')
child.expect('Choose action to perform on "src/dir3"')
child.sendline('R')
child.expect(pexpect.EOF)

action_remove_common.check_dummy_tree("src", {"dir2" : {}})

action_remove_common.rm_fr("src")
action_remove_common.rm_fr("dest")
sys.exit(0)
