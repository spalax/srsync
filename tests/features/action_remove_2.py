import pexpect
import stat
import sys
import os

import action_remove_common

homedir = os.getcwd()
env = {"HOME": homedir, "SHELL": "sh", "PAGER": "cat"}
srsync = os.path.abspath("../../srsync") + " "

action_remove_common.rm_fr("src")
action_remove_common.rm_fr("dest")
original = {"src" : {"dir1" : {'': ['1', '2'], 'dir':{}}, "dir2" : {}, "dir3" : {'': ["1", "2"]}}, "dest":{}}
action_remove_common.create_dummy_tree(original)
os.chmod("src/dir1", stat.S_IRUSR | stat.S_IXUSR)

child = pexpect.spawn(srsync + 'exec print --one-shot --shell-output=stdout src dest', env = env)
child.logfile = sys.stdout;

child.expect('Choose action to perform on "src"')
child.sendline('d')
child.expect('Choose action to perform on "src/dir1"')
child.sendline('r')
child.expect('Are you sure you want to remove "src/dir1"')
child.sendline('y')
i = -1
while True:
    i = child.expect(["Choose action to perform on", pexpect.EOF, "rsync"])
    if i == 0:
        child.sendline('s')
    elif i == 1:
        sys.exit(1)
    elif i == 2:
        break

os.chmod("src/dir1", stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
success = action_remove_common.check_dummy_tree("src", original["src"])

action_remove_common.rm_fr("src")
action_remove_common.rm_fr("dest")

if success:
    sys.exit(0)
else:
    sys.exit(1)
