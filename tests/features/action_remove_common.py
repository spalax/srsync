import os
import stat

def create_dummy_tree(tree):
    for directory in tree.keys():
        if directory == "":
            for sub in tree[directory]:
                open(sub, 'a').close()
        else:
            os.mkdir(directory)
            cwd = os.getcwd()
            os.chdir(directory)
            create_dummy_tree(tree[directory])
            os.chdir(cwd)

def rm_fr(path):
    if not os.path.exists(path):
        return
    os.chmod(path, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
    if os.path.isdir(path):
        for sub in os.listdir(path):
            rm_fr(os.path.join(path, sub))
        os.rmdir(path)
    else:
        os.remove(path)

def check_dummy_tree(tree, controltree):
    n = 0
    while (os.path.exists("control%s" % n)):
        n += 1
    controlname = "control%s" % n
    os.mkdir(controlname)
    cwd = os.getcwd()
    os.chdir(controlname)
    create_dummy_tree(controltree)
    os.chdir(cwd)
    rc = os.system("diff --recursive %s %s" % (tree, controlname))
    rm_fr(controlname)
    return rc == 0
