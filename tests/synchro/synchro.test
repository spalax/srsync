import os
homedir = os.getcwd()
env = {"HOME": homedir }
srsync = os.path.abspath("../../srsync") + " "
success = Template(expected = 0,
        env = env, save_stderr = True, save_stdout = True, shell = True)
fail = Template(expected = 1,
        env = env, save_stderr = True, save_stdout = True, shell = True)

def checkTree(task, base):
    # Check that the obtained tree is right: everithing we wanted to be copied
    # has been copied, and nothing more
    task.post += DirExists(base + "perhaps1")
    task.post += DirExists(base + "perhaps2")
    task.post += DirExists(base + "no1")
    task.post += DirExists(base + "no2")
    task.post += DirExists(base + "yes1")
    task.post += DirExists(base + "yes3")
    task.post += DirExists(base + "yes2")
    task.post += Not(DirExists(base + "perhaps3"))
    task.post += Not(DirExists(base + "no3"))
    task.post += FileExists(base + "perhaps2/yes1")
    task.post += FileExists(base + "no1/yes1")
    task.post += FileExists(base + "no1/yes2")
    task.post += FileExists(base + "no2/yes1")
    task.post += FileExists(base + "yes1/yes1")
    task.post += FileExists(base + "yes1/yes2")
    task.post += FileExists(base + "perhaps1/yes1")
    task.post += FileExists(base + "perhaps1/yes2")
    task.post += FileExists(base + "yes2/yes1")
    task.post += Not(FileExists(base + "no2/no1"))
    task.post += Not(FileExists(base + "no3/no1"))
    task.post += Not(FileExists(base + "no3/no2"))
    task.post += Not(FileExists(base + "perhaps2/no1"))
    task.post += Not(FileExists(base + "perhaps3/no1"))
    task.post += Not(FileExists(base + "perhaps3/no2"))
    task.post += Not(FileExists(base + "yes2/no1"))
    task.post += Not(FileExists(base + "yes3/no1"))
    task.post += Not(FileExists(base + "yes3/no2"))

### Set up
Command("rm -fr dest/*", shell=True)
Command('rm -fr .srsync')
Command(srsync + "mark accept $(find . -name 'yes?')", template = [success])
Command(srsync + "mark reject $(find . -name 'no?')", template = [success])
Command(srsync + "mark descend src $(find . -name 'perhaps?')", template = [success])

### Simple copy
Command("rm -fr dest/*", shell=True)
simple_copy = Test(template=[success],
        cmd=srsync + "rsync src dest",
        desc="Simple copy")
simple_copy.post += FileEquals("empty", simple_copy.stdout)
simple_copy.post += FileEquals("empty", simple_copy.stderr)
checkTree(simple_copy, "dest/src/")

### Multiple source dirs
Command("rm -fr dest/*", shell=True)
multiple_src = Test(template=[success],
        cmd=srsync + "rsync src/* dest",
        desc="Multiple source")
multiple_src.post += FileEquals("empty", multiple_src.stdout)
multiple_src.post += FileEquals("empty", multiple_src.stderr)
checkTree(multiple_src, "dest/")

### Different origin
Command("rm -fr dest/*", shell=True)
different_origin1 = Test(template=[success],
        cwd="src",
        cmd=srsync + "rsync -o .. * ../dest",
        desc="Different origins")
different_origin1.post += FileEquals("empty", different_origin1.stdout)
different_origin1.post += FileEquals("empty", different_origin1.stderr)
checkTree(different_origin1, "dest/src/")

Command("rm -fr dest/*", shell=True)
different_origin2 = Test(template=[success],
        cwd="..",
        cmd=srsync + "rsync -o synchro synchro/src/* synchro/dest",
        desc="Different origins")
different_origin2.post += FileEquals("empty", different_origin2.stdout)
different_origin2.post += FileEquals("empty", different_origin2.stderr)
checkTree(different_origin2, "dest/src/")

### Cleaning
Command("rm -fr dest/*", shell=True)
Command('rm -fr .srsync')
