Usage: srsync [exec ACTION|rsync] [--help] [SOURCE...] DEST [OPTIONS] [--options RSYNC_OPTIONS]
Update configuration and synchronise files.

Two subcommands are available:
  exec ACTION	See below.
  rsync      	Shortcut for "exec rsync".

The following actions are available:
  rsync		Update configuration and synchronise files.
  print		Update configuration and print the command that would have been executed with "exec rsync".

Arguments:
Note: SOURCE, DEST and OPTIONS can be mixed, as long as DEST appears after SOURCE.
Note: --options must be the last option on the list. Content of RSYNC_OPTIONS is not analysed by srsync.
  SOURCE       	paths to synchronize with destination paths. They must be local files.
  DEST         	destination path. It can be anything supported by rsync.
  OPTIONS      	srsync options. See below.
  RSYNC_OPTIONS	rsync options. They are not processed and given directly to rsync.

Update options:
  -h [ --help ]                    Print help and exit.
  -b [ --absolute ]                refer to paths by their absolute name
  -q [ --quiet ]                   do not print warnings about incoherent 
                                   configuration
  -p [ --purge ]                   forget references to non-existent paths.
  -P [ --profile ] arg (=default)  use profile 'arg' (default is "default").
  --no-ask [=arg(=n)]              do not ask what to do with not-handled 
                                   paths. If an optional argument is set, apply
                                   its action (default is 'n').
                                   y accept path
                                   n reject path
  -1 [ --one-shot ]                do not save changes
  --shell-output arg (=default)    set the output of a shell that might be 
                                   invoked while running srsync. This may be 
                                   useful when output of srsync is stored in a 
                                   variable, or passed by a pipe to another 
                                   command. Available values are:
                                   none: shell output is discarded;
                                   stdout: shell output is printed on standard 
                                   output;
                                   stderr: shell output is printed on standard 
                                   error;
                                   default: shell output is printed on default 
                                   output.
  --bin arg                        binary to use instead of the default one 
                                   (most likely 'rsync') when performing 
                                   synchronisation
  -c [ --careful ]                 print the command line to be executed and 
                                   ask user to confirm before actually 
                                   performing synchronisation
  -o [ --origin ] arg              define an alternative origin base folder
  --options                        options to be passed to rsync.
