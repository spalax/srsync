Usage: srsync mark [--help] ACTION PATHS
Set the status of paths given in argument.

The following actions are available:
  accept 	Mark the paths for being accepted.
  ask    	Mark the paths for action 'ask'.
  descend	Mark the paths for action 'descend'.
  forget 	Forget configuration of path.
  reject 	Mark the paths for not being accepted.


Update options:
  -h [ --help ]                    Print help and exit.
  -b [ --absolute ]                refer to paths by their absolute name
  -q [ --quiet ]                   do not print warnings about incoherent 
                                   configuration
  -p [ --purge ]                   forget references to non-existent paths.
  -P [ --profile ] arg (=default)  use profile 'arg' (default is "default").
  -f [ --force ]                   do not ask before overwriting an existing 
                                   configuration, and accept setting the status
                                   of non-existing paths.
  -i [ --interactive ]             ask before overwriting an existing 
                                   configuration (default).
  -e [ --explicit ] [=arg(=ynad)]  only consider files that are explicitly 
                                   marked. If an optional argument is given, 
                                   only consider files that are marked as 
                                   specified by this argument: respectively y, 
                                   n, d, a, x for paths marked as accepted, 
                                   rejected, descend, ask, not marked.
                                   Combinations are allowed, e.g. "-e yx" will 
                                   only consider paths that are not marked, or 
                                   marked as accepted.
  -m [ --marked ] [=arg(=ynad)]    only consider files that are explicitly or 
                                   implicitely marked. If an optional argument 
                                   is given, only consider files that are 
                                   marked as specified by this argument: 
                                   respectively y, n, d, a, x for paths marked 
                                   as accepted, rejected, descend, ask, not 
                                   marked.
                                   Combinations are allowed, e.g. "-m yx" will 
                                   only consider paths that are not marked, or 
                                   marked as accepted.
  -r [ --recursive ]               recursively perform modes on paths.
