Usage: srsync status [OPTIONS] PATHS
Print status of paths given in argument.

Update options:
  -h [ --help ]                    Print help and exit.
  -b [ --absolute ]                refer to paths by their absolute name
  -q [ --quiet ]                   do not print warnings about incoherent 
                                   configuration
  -p [ --purge ]                   forget references to non-existent paths.
  -P [ --profile ] arg (=default)  use profile 'arg' (default is "default").
  -e [ --explicit ] [=arg(=ynad)]  only consider files that are explicitly 
                                   marked. If an optional argument is given, 
                                   only consider files that are marked as 
                                   specified by this argument: respectively y, 
                                   n, d, a, x for paths marked as accepted, 
                                   rejected, descend, ask, not marked.
                                   Combinations are allowed, e.g. "-e yx" will 
                                   only consider paths that are not marked, or 
                                   marked as accepted.
  -m [ --marked ] [=arg(=ynad)]    only consider files that are explicitly or 
                                   implicitely marked. If an optional argument 
                                   is given, only consider files that are 
                                   marked as specified by this argument: 
                                   respectively y, n, d, a, x for paths marked 
                                   as accepted, rejected, descend, ask, not 
                                   marked.
                                   Combinations are allowed, e.g. "-m yx" will 
                                   only consider paths that are not marked, or 
                                   marked as accepted.
  -u [ --useful ]                  with --status and -r, only print useful 
                                   paths: do not print subpaths of a directory 
                                   in which every subpath inherits its status 
                                   from its parent
  -r [ --recursive ]               recursively perform modes on paths.
