/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <stdarg.h>

#include"global.h"

namespace my_system {
  /*
   * Convert the list of arguments, which are supposed to be strings, into an
   * array of char*.
   * Arguments:
   * - count: number of arguments (without "count" itself).
   * - remaining arguments: strings.
   *
   * The returned array must be freed at some point to prevent memory leaks.
   */
  char** array_of_char_star(int count, ...);

  /*
   * Run a command.
   * Arguments:
   * - command: (relative or absolute) path to the command to run.
   * - arguments: arguments for the command.
   * - dir: working directory of the command to be executed.
   * - output_file: file in which to redirect both standard and error output.
   * Remark: Arguments "command" and "arguments" will be passed without any
   *   modification to "execvp".
   * Returns:
   * 0 iff command was executed without error. Something else if errors occured.
   */
  int safe_exec(
      char *command, char*arguments[],
      char* dir = NULL, char* output_file = NULL);
}
