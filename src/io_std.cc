/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <set>

#include "io_std.h"
#include "my_system.h"

/*
 * Null stream.
 */
nullstreambuf globalnullstreambuf;
std::ostream globalnullstream(&globalnullstreambuf);

/*
 * Streams
 */
std::ostream& print_trace = std::cout;
std::ostream& print_error = std::cerr;
std::ostream& print_null = globalnullstream;
std::ostream& print_true_standard = std::cout;

output_type output = standard;
void set_standard_output(output_type out) {
  output = out;
}
std::ostream& print_standard() {
  switch(output) {
    case null:
      return globalnullstream;
    case error:
      return std::cerr;
    case standard:
      return std::cout;
  }
  // Useless, but compiler complains
  return std::cout;
}

/*
 * If argument is a file, remove it. If it is a directory, remove its content,
 * and the directory.
 * Acts like boost::filesystem::remove_all(), excepted that files that could
 * not be deleted are printed on output.
 *
 * Returns true iff all files were deleted.
 */
bool my_remove_all(const fs::path& p) {
  bool success = true;
  if (fs::is_directory(p)) {
    fs::directory_iterator itr, end_itr;
    for (itr = fs::directory_iterator(p);
        itr != end_itr;
        ++itr)
      success &= my_remove_all(itr->path());
  }
  if (success) {
    try {
      fs::remove(p);
    } catch (fs::filesystem_error&) {
      print_error << "Could not delete \"" << p << "\"." << endl;
      return false;
    }
  }
  return success;
}


char io_std::choose_char(int number, char list[], char def) {
  set<char> chars; // Set of possible choices
  string available; // String representing this set

  // We put the arguments in a set
  int i;
  for(i = 0; i < number; i++) {
    chars.insert(list[i]);
    available.push_back(list[i]);
    available.push_back('/');
  }
  available.erase(available.length() - 1); // Deleting last '/'

  // Reading user answer
  string answer;
  bool eos; // End of stream?
  while (answer.length() != 1 || chars.count(answer[0]) == 0) {
    eos = static_cast<bool>(cin >> answer);
    if (!eos) {
      print_standard() << endl;
      return def;
    }
    if (answer.length() != 1 || chars.count(answer[0]) == 0) {
      print_standard() << "Answer one of [" << available << "]: ";
    }
  }

  return answer[0];
}

bool io_std::askOverwriteAction(fs::path p, action o, action n) {
  print_standard() << "Overwrite action \"" << o << "\" of path \"" << p
    << "\" with new action \"" << n << "\"? [y/n] ";
  char choice[] = {'y', 'n'};
  return (choose_char(2, choice, 'n') == 'y');
}

bool io_std::askConfirmRemove(fs::path p) {
  print_standard() << "Are you sure you want to remove \"" << p.string() << "\"";
  if (fs::is_directory(p))
    print_standard() << " (and its content)";
  print_standard() << "? [y/n] ";
  char choice[] = {'y', 'n'};
  return (choose_char(2, choice, 'n') == 'y');
}

action io_std::askChooseAction(fs::path p, action current, string shell_output) {
  char choice[] = {'y', 'n', 'd', 'a', 's', 'l', '$', 'r', 'R', 'h'};
  char c;
  bool end = false;
  bool remove;
  char *shell, *dir, *pager, *output_file;
  /*
   * Some commands need variable "output_file" to be set, to know where to
   * print stuff.
   * This is done so that output that is not a command is printed to standard
   * error when calling "srsync exec print ...".
   */
  if (shell_output == "default")
    switch(output) { // Global variable defined at the beginning of this file
      case null:
        shell_output = "none";
        break;
      case error:
        shell_output = "stderr";
        break;
      case standard:
        shell_output = "stdout";
        break;
    }
  output_file = NULL;
  if (shell_output == "stdout")
    output_file = (char*) "/dev/stdout";
  else if (shell_output == "stderr")
    output_file = (char*) "/dev/stderr";
  else if (shell_output == "none")
    output_file = (char*) "/dev/null";

  while(! end) {
    print_standard() << "Choose action to perform on \"" << p.string()
      << "\" [y/n/d/a/s/l/$/r/R/h]. ";
    c = choose_char(10, choice, 's');
    switch(c) {
      case 'h':
        print_standard() << "y: accept path" << endl;
        print_standard() << "n: reject path" << endl;
        print_standard() << "d: descend (no default action for subpaths; " <<
          "directory only)" << endl;
        if (current == a_ask)
          print_standard() << "a: stop asking" << endl;
        else
          print_standard() << "a: always ask for action" << endl;
        print_standard() << "s: skip (ignore this time only)" << endl;
        print_standard() << "l: print content of path" << endl;
        print_standard() << "$: run a shell to let you examine content" << endl;
        print_standard() << "r: remove file (or directory)." << endl;
        print_standard() << "R: remove file (or directory), without asking " <<
          "confirmation first." << endl;
        print_standard() << "h: print this help" << endl;
        break;
      case 'l':
        if (fs::is_directory(p)) {
          // Executing "ls -l DIRECTORY"
          if (0 != my_system::safe_exec((char*) "ls",
                my_system::array_of_char_star(3,
                  "ls", "-l", p.string().c_str()),
                (char*) ".", output_file))
            print_error
              << "Error while launching external command \"ls\"" << endl;
        } else {
          // If $PAGER is defined, use it to print content of FILE; if not, use
          // "more".
          pager = getenv("PAGER");
          if (pager == NULL)
            pager = (char*) "more";
          if (0 != my_system::safe_exec(pager, my_system::array_of_char_star(2,
                  pager, p.string().c_str()), (char*) ".", output_file))
            print_error
              << "Error while launching external command \"" << pager << "\""
              << endl;
        }
        break;
      case '$':
        print_standard() << "Type 'exit' to come back to srsync." << endl;
        shell = getenv("SHELL");
        if (shell == NULL)
          shell = (char*) "sh";
        if (fs::is_directory(p))
          dir = (char*) p.string().c_str();
        else
          dir = (char*) p.parent_path().string().c_str();
        if (0 != my_system::safe_exec(shell,
              my_system::array_of_char_star(1, shell), dir, output_file))
          print_error
            << "Error while launching external command \"" << shell << "\""
            << endl;
        break;
      case 'r':
      case 'R':
        // Preparing
        remove = false;
        // Asking confirmation
        if (c == 'R') {
          remove = true;
        } else if (askConfirmRemove(p)) {
          remove = true;
        }
        // Removing
        if (remove) {
          end = my_remove_all(p);
          if (not end)
            print_error << "Some files could not be deleted. You may try " <<
              "to solve this manually (checking file permissions for " <<
              "example) by opening a shell ($)." << endl;
        }
        break;
      default:
        end = true;
        break;
    }
  }

  switch(c) {
    case 'y':
      return a_accept;
    case 'n':
      return a_reject;
    case 'd':
      return a_descend;
    case 'a':
      return a_ask;
    case 's':
      return a_none;
    case 'r':
    case 'R':
      return a_none;
    default: // Impossible
      return a_none;
  }
}

bool io_std::askConfirmExecute() {
  print_standard() << "Are you sure yout want to execute command(s)? [y/n] ";
  char choice[] = {'y', 'n'};
  return (choose_char(2, choice, 'n') == 'y');
}
