#!/bin/bash
# Use it as you would use "srsync"
# Run srsync, and print the changes made to the profile file "default".

sort ~/.srsync/default > ~/.srsync/.default.bak
./srsync "$@"
sort ~/.srsync/default | diff ~/.srsync/.default.bak -
rm ~/.srsync/.default.bak
