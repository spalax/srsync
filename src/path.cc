/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#include<queue>

#include"path.h"
#include"io_std.h"
#include"options.h"
using namespace path;

/*****************************************************************************
 * iterator
 ****************************************************************************/

// Constructors
Iterator::Iterator(Path *p):
  current_path(p),
  endMark(false),
  recursive(NULL)
{
}

// Destructors
Iterator::~Iterator() {
}

// Content of the iterator
Path* Iterator::operator*() {
  if (this->recursive == NULL)
    return this->current_path;
  else
    return **(this->recursive);
}

// Incrementation
Iterator Iterator::operator++(int) {
  bool new_recursive = false;

  // Test whether this path has been treated or not
  if (this->recursive == NULL) {
    this->children = this->current_path->children.begin();
    skipDots(this->children, this->current_path->children.end());
    new_recursive = true;
  } else {
    (*this->recursive)++;
    if (this->recursive->over()) {
      delete this->recursive;
      this->children++;
      skipDots(this->children, this->current_path->children.end());
      new_recursive = true;
    }
  }

  // All subpaths have been treated?
  if (this->children == this->current_path->children.end()) {
    this->endMark = true;
    new_recursive = false;
  }

  if (new_recursive)
    this->recursive = new Iterator(this->children->second);

  return *this;
}

bool Iterator::over() const {
  return this->endMark;
}

/*****************************************************************************
 * Status iterator
 ****************************************************************************/
// Constructors
StatusIterator::StatusIterator(Path *p, action a):
  stupid(false),
  custom_path(p),
  act(a),
  endMark(false),
  recursive(NULL)
{
  children = fs::directory_iterator();
}

StatusIterator::StatusIterator(fs::path p, action a):
  stupid(true),
  fs_path(p),
  act(a),
  endMark(false),
  recursive(NULL)
{
  children = fs::directory_iterator();
}

// Destructors
StatusIterator::~StatusIterator() {
}

// Content of the iterator
struct statusData StatusIterator::operator*() {
  if (this->children != fs::directory_iterator()) {
    return **(this->recursive);
  }

  // Returning current path
  struct statusData ret;
  if (stupid) {
    ret.path = this->fs_path;
    ret.act = act;
    ret.inherited = (act != a_none);
  } else {
    ret.path = this->custom_path->path;
    ret.act = this->custom_path->actualAction(false);
    ret.inherited =
      (this->custom_path->actualAction(false)
       == this->custom_path->inherited_action);
  }
  return ret;
}

// Incrementation
StatusIterator StatusIterator::operator++(int) {
  bool new_recursive = false;

  // Test whether this path has been treated or not
  if (this->children == fs::directory_iterator()) {
    if (Options::global->GetRecursive()) {
      // Iterating this path's subpaths (if the path is a directory
      try{
        if (this->stupid) {
          if (fs::is_directory(this->fs_path))
            if (!Options::global->GetUseful()) // Handling option --useful
              this->children = fs::directory_iterator(this->fs_path);
        } else {
          if (fs::is_directory(this->custom_path->path))
            if ((!Options::global->GetUseful()) ||
                (Options::global->GetUseful() &&
                 !this->custom_path->AllImplicit())) // Handling option --useful
              this->children = fs::directory_iterator(this->custom_path->path);
        }
        new_recursive = true;
      }
      catch(fs::filesystem_error& e) {
        if (!Options::global->GetQuiet()) {
          print_error << "srsync: Warning: Could not stat \"";
          if (this->stupid)
            print_error << this->fs_path;
          else
            print_error << this->custom_path->path;
          print_error << "\". Subpaths of it are ignored." << endl;
        }
      }
    }
  } else {
    // Treating this path
    (*this->recursive)++;
    if (this->recursive->over()) {
      this->children++;
      delete this->recursive;
      new_recursive = true;
    }
  }

  // All subpaths have been treated?
  if (this->children == fs::directory_iterator()) {
    this->endMark = true;
    new_recursive = false;
  }

  // Need to create a new iterator for subpaths?
  if (new_recursive) {
    string next = (*this->children).path().filename().string();
    if (!this->stupid)
      if (this->custom_path->children.count(next)) {
        this->recursive = new StatusIterator(
            this->custom_path->children[next],
            this->custom_path->actualAction(true));
        return *this;
      }
    if (this->stupid)
      this->recursive = new StatusIterator(this->fs_path / next, this->act);
    else
      this->recursive = new StatusIterator(
          this->custom_path->path / next,
          this->custom_path->actualAction(true));
  }

  return *this;
}

bool StatusIterator::over() const {
  return this->endMark;
}

/*****************************************************************************
 * Class Path
 ****************************************************************************/
/*
 * Constructor
 */
Path::Path(fs::path p, path::Path* parent, bool d, action a, action i):
  path(p),
  act(a),
  dir(d),
  inherited_action(i) {
    this->children["."] = this;
    if (parent != NULL)
      this->children[".."] = parent;
    else
      this->children[".."] = this;
  }

/*
 * Destructor
 */
Path::~Path(){
  map<string, Path*>::iterator it;
  for(it = this->children.begin(); it != this->children.end(); it++)
    if ((*it).first != "." && (*it).first != "..")
      delete((*it).second);
}

/*
 * Add path to structure
 *
 * Either 'l' is empty, in which case we do create the object, or 'l' is not.
 * If not, we have to check if objects leading to this path exist. If not, we
 * create them.
 */
Path* Path::addSub(list<string> l, action a, fs::path user) {
  // Removing leading "/" (if relevant)
  if (!l.empty())
    if (l.front() == "/")
      l.pop_front();

  if (!l.empty()) {
    fs::path p = stringsToPath(l);
    string first = l.front();
    l.pop_front();
    if (! this->children.count(first)) {
      this->children[first] = new Path(
          this->path / first,
          this,
          fs::is_directory(this->path / first),
          a_none,
          this->actualAction(true));
    }
    return this->children[first]->addSub(l, a, user);
  } else {
    this->dir = fs::is_directory(this->path);
    if (user != fs::path())
      this->path = user;
    if (! this->checkMarked())
      return this;
    if (a != a_none) {
      if (this->act != a && this->act != a_none && !Options::global->GetForce())
        if (! io_std::askOverwriteAction(this->path, this->act, a))
          return NULL;
      this->act = a;
      try{
        if (Options::global->GetRecursive() && this->dir)
          this->buildAllSub(a);
      }
      catch(Options::Exception_WrongSubcommand) {
      }
    }
    return this;
  }
}

/*
 * Build objects for all subpaths of this.
 */
void Path::buildAllSub(action a) {
  fs::directory_iterator end_itr; // default construction yields past-the-end
  for (fs::directory_iterator itr(this->path); itr != end_itr; ++itr) {
    list<string> sub;
    sub.push_back(itr->path().filename().string());
    this->addSub(sub, a);
  }
}

/*
 * Return the action or inherited action of 'this'.
 */
action Path::actualAction(bool rest) {
  action value;
  if (this->act == a_none)
    value = this->inherited_action;
  else
    value = this->act;
  if (rest and ! (value == a_accept || value == a_reject))
    value = a_none;
  return value;
}

/*
 * Return true if the path represented by the argument is considered.
 */
bool Path::checkMarked(list<string> p) {
  if (p.empty()) {
    return Options::global->IsConsidered(this->act, this->actualAction(true));
  } else {
    if (this->children.count(p.front())) {
      p.pop_front();
      string first = p.front();
      return this->children[first]->checkMarked(p);
    } else {
      return Options::global->IsConsidered(a_none, this->actualAction(true));
    }
  }
}

/*
 * Return an iterator over the tree of Path.
 */
Iterator Path::Iteratorbegin() {
  return Iterator(this);
}

/*
 * Return a list of 'struct statusData' which is the list of paths to deal with.
 * This function is a wrapper for private method AcceptedSubpathList, and
 * simply add the current path to the list, if relevant.
 */
list<struct statusData> Path::AcceptedList() {
  action a;
  list<struct statusData> ret = this->AcceptedSubpathList(a);
  if (Options::global->cl_subcommand == Options::subcommand_print) {
    if ( (a == a_accept
          && Options::global->GetOutput() == Options::type_print_list)
        || (a == a_reject
          && Options::global->GetOutput() == Options::type_print_nlist)) {
      struct statusData tmp;
      tmp.path = this->path;
      tmp.act = a;
      ret.push_back(tmp);
    }
  } else if (Options::global->cl_subcommand == Options::subcommand_exec) {
    struct statusData tmp;
    tmp.path = this->path;
    if (a == a_none)
      tmp.act = a_accept;
    else
      tmp.act = a;
    ret.push_front(tmp);
  }

  return ret;
}

/*
 * For each element 'e' of list 'n', create a corresponding 'statusData'
 * structure, with action 'act', path 'base / e', and append this structure
 * to list 'l'.
 * If 'star' is set, add a second element with path 'base / e / ***'.
 */
void appendList(
    list<struct statusData> &l, list<string> n, action act, fs::path base,
    bool star) {
  for(list<string>::iterator it = n.begin(); it != n.end(); it++) {
    if (star) {
      struct statusData tmp;
      tmp.path = base / *it / "***";
      tmp.act = act;
      l.push_front(tmp);
    }
    struct statusData tmp;
    tmp.path = base / *it;
    tmp.act = act;
    l.push_front(tmp);
  }
}

/*
 * Return the list of paths to deal with, and set 'act' to be the action
 * performed on every subpath of this->path, that is:
 */
list<struct statusData> Path::AcceptedSubpathList(action &act) {
  list<struct statusData> ret;

  /*
   * Asking user, if necessary, until action is one of a_accept, a_reject,
   * a_descend.
   */
  action old_action = this->act;
  action temp_action = this->actualAction(false);
  while(temp_action == a_none || temp_action == a_ask) {
    if (Options::global->GetNoAsk() == a_none)
      temp_action = io_std::askChooseAction(this->path, temp_action, Options::global->GetShellOutput());
    else
      temp_action = Options::global->GetNoAsk();

    switch(temp_action) {
      case a_ask:
        if (this->actualAction(false) == a_ask) {
          this->act = a_none;
          temp_action = a_none;
        } else {
          this->act = a_ask;
          temp_action = a_ask;
        }
        break;
      case a_none:
        temp_action = a_reject;
        break;
      default:
        if (this->actualAction(false) != a_ask) {
          this->act = temp_action;
          temp_action = this->actualAction(false);
        }
        break;
    }
  }
  if (temp_action == a_descend)
    temp_action = a_none;
  /*
   * Now, temp_action is one of a_accept, a_reject, a_none
   * - a_accept: accept this directory
   * - a_reject: reject this directory
   * - a_none: descend
   */

  if (old_action != this->act)
    this->PropagateAction();

  /*
   * 'accepted', 'rejected' and 'mixed' will contain respectively the list of
   * children names that are respectively accepted, rejected, or have some
   * subpaths accepted, and some other rejected.
   * Here, "accepted" means "every subpath (and recursively) are accepted", and
   * "rejected" means "at least one of its subpath is not accepted".
   */
  list<string> accepted, rejected, mixed;

  /* 
   * The two conditions below may seem redundant, but they are not:
   * "this->path" may no longer be a directory since the moment we first
   * analysed it.
   */
  if (this->dir && fs::is_directory(this->path)) {
    /*
     * Sorting subpaths alphabetically
     */
    priority_queue<string, vector<string>, greater<string> > subpaths;
    fs::directory_iterator end_itr; // default construction yields past-the-end
    for (fs::directory_iterator itr(this->path); itr != end_itr; ++itr)
      subpaths.push(itr->path().filename().string());
    /*
     * Sorting done
     */

    while(!subpaths.empty()) {
      if (this->children.count(subpaths.top()) == 0
          && temp_action != a_none) {
        switch(temp_action){
          case a_accept:
            accepted.push_back(subpaths.top());
            break;
          case a_reject:
            rejected.push_back(subpaths.top());
            break;
          default: // Impossible
            break;
        }
      } else {
        if (this->children.count(subpaths.top()) == 0)
            this->addSub(list<string>(1, subpaths.top()), a_none);
        if (temp_action != a_none
            && this->children[subpaths.top()]->AllImplicit()) {
          if (this->children[subpaths.top()]->act == a_accept)
            accepted.push_back(subpaths.top());
          if (this->children[subpaths.top()]->act == a_reject)
            rejected.push_back(subpaths.top());
        } else {
          action children_action;
          list<struct statusData> sub = this->children[subpaths.top()]
            ->AcceptedSubpathList(children_action);
          list<struct statusData>::iterator it;
          for(it = sub.begin(); it != sub.end(); it++)
            ret.push_back(*it);
          if (children_action == a_accept)
            accepted.push_back(subpaths.top());
          if (children_action == a_reject)
            rejected.push_back(subpaths.top());
          if (children_action == a_none)
            mixed.push_back(subpaths.top());
        }
      }
      subpaths.pop();
    } 

    if (Options::global->cl_subcommand == Options::subcommand_print) {
      if (Options::global->GetOutput() == Options::type_print_list) {
        if (temp_action != a_accept || (! rejected.empty())) {
          list<string>::iterator it;
          for(it = accepted.begin(); it != accepted.end(); it++) {
            struct statusData tmp;
            tmp.path = this->path / *it;
            tmp.act = a_accept;
            ret.push_back(tmp);
          }
          act = a_reject;
        } else {
          act = a_accept;
        }
      } else if (Options::global->GetOutput() == Options::type_print_nlist) {
        if (temp_action != a_reject || (! accepted.empty())) {
          list<string>::iterator it;
          for(it = rejected.begin(); it != rejected.end(); it++) {
            struct statusData tmp;
            tmp.path = this->path / *it;
            tmp.act = a_reject;
            ret.push_back(tmp);
          }
          act = a_accept;
        } else {
          act = a_reject;
        }
      }
    } else if (Options::global->cl_subcommand == Options::subcommand_exec) {
      /*
       * The explanations of the above lines (the one contained in the same
       * {...} as this comment) are quite long, and explained in file
       * "path.comment".
       */
      if ((temp_action == a_accept
            && mixed.empty()
            && accepted.empty()
            && rejected.empty())
          || ((! accepted.empty())
            && rejected.empty()
            && mixed.empty())) {
        act = a_accept;
      } else if (temp_action != a_accept
          && (accepted.empty()
            && mixed.empty())) {
        act = a_reject;
      } else {
        act = a_none;
      }

      if ((! accepted.empty()) && mixed.empty() && rejected.empty()) {
        appendList(ret, accepted, a_accept, this->path, false);
      }
      if (! mixed.empty()) {
        appendList(ret, mixed, a_accept, this->path, false);
      }
      if ((! accepted.empty()) && ((! mixed.empty()) || (! rejected.empty()))) {
        appendList(ret, accepted, a_accept, this->path, true);
      }
      if ((! rejected.empty())
          && ! ((temp_action == a_none || temp_action == a_reject)
            && accepted.empty() && mixed.empty()
            && (! rejected.empty()))) {
        appendList(ret, rejected, a_reject, this->path, false);
      }
    }

  } else {
    act = temp_action;
  }

  return ret;
}

/*
 * Return an iterator over the list of (real) paths, with their status.
 */
StatusIterator Path::statusIteratorbegin(list<string> p) {
  if (p.empty())
    return StatusIterator(this, this->actualAction(true));
  else
    if (this->children.count(p.front())) {
      string first = p.front();
      p.pop_front();
      return this->children[first]->statusIteratorbegin(p);
    } else {
      return StatusIterator(path::stringsToPath(p), this->actualAction(true));
    }
}

/*
 * Return true iff all subpaths inherit their action from this path.
 */
bool Path::AllImplicit() {
  action a = this->actualAction(false);
  if (a != a_accept && a != a_reject && a != a_none)
    return false;

  map<string, Path*>::iterator it;
  for(it = this->children.begin(); it != this->children.end(); it++)
    if (it->first != "." && it->first != "..") {
      if ((*it).second->actualAction(false) != a)
        return false;
      if (!(*it).second->AllImplicit())
        return false;
    }

  return true;
}

/*
 * Propagate name
 */
void Path::PropagateName() {
  map<string, Path*>::iterator it;
  for(it = this->children.begin(); it != this->children.end(); it++)
    if (it->first != "." && it->first != "..") {
      it->second->path = this->path / it->first;
      it->second->PropagateName();
    }
}

/*
 * Propagate action
 */
void Path::PropagateAction() {
  map<string, Path*>::iterator it;
  for(it = this->children.begin(); it != this->children.end(); it++)
    if (it->first != "." && it->first != "..")
      if (it->second->inherited_action != this->actualAction(true)) {
        it->second->inherited_action = this->actualAction(true);
        it->second->PropagateAction();
  }
}

/*
 * Print tree of paths stored in "this".
 */
void Path::printTree(int depth) {
  map<string, Path*>::iterator it;

  string prefix = "";
  int d;
  for(d = depth; d>0; d--)
    prefix += " ";

  for(it = this->children.begin(); it != this->children.end(); it++)
    if (it->first != "." && it->first != "..") {
      print_standard() << prefix << it->first << " (" << it->second->path
        << ")" << endl;
      it->second->printTree(depth+1);
    }
}



/*******************************************************************************
 * Function
 ******************************************************************************/
/*
 * Convert a path to a list of strings
 */
list<string> path::pathToStrings(fs::path p) {
  list<string> l;
  fs::path::iterator it;
  for(it = p.begin(); it != p.end(); it++)
    l.push_back(it->string());
  return l;
}

/*
 * Converse a list of strings to a path
 */
fs::path path::stringsToPath(list<string> s) {
  fs::path p;
  list<string>::iterator it;
  for(it = s.begin(); it != s.end(); it++)
    p = p / *it;
  return p;
}

/*
 * Remove "." directories.
 */
fs::path path::purge(fs::path p) {
  fs::path n = fs::path();
  p = fs::system_complete(p);
  for(fs::path::iterator it = p.begin(); it != p.end(); ++it) {
    if (*it == ".") {
      ;
    } else if (*it == "..") {
      if (n != n.root_path())
        n = n.parent_path();
    } else {
      n = n / *it;
    }
  }
  n.root_name() = p.root_name();
  n.root_directory() = p.root_directory();
  return n;
}

/*
 * Given an iterator map<string, Path*>::iterator over the children of a
 * Path, iterate it (if necessary) to skip "." and ".." children.
 */
map<string, Path*>::iterator path::skipDots(
    map<string, Path*>::iterator &it, map<string, Path*>::iterator end) {
    if (it == end)
      return it;
    if (it->first == "." || it->first == "..") {
        it++;
        return skipDots(it, end);
    }
    return it;
}

/*
 * Return true iff 'sub' is a subpath of 'p' (i.e. iff there exists a path 'q'
 * such that "p = sub/q").
 * Convention: for any 'p', subpath("", p) returns true.
 */
bool path::subpath(string sub, string p) {
  if (sub == "")
    return true;

  fs::path p_sub = purge(sub);
  fs::path p_p = purge(p);

  fs::path::iterator it_sub, it_p;
  for(
      it_sub = p_sub.begin(), it_p = p_p.begin();
      it_sub != p_sub.end();
      it_sub++, it_p++) {
    if (it_p == p_p.end())
      return false;
    if (*it_sub != *it_p)
      return false;
  }
  return true;
}

/*
 * Returns path 'full', without its prefix 'prefix'.
 * Precondition: subpath(prefix, full) == true
 */
fs::path path::remove_prefix(fs::path prefix, fs::path full) {
  fs::path ret;
  fs::path::iterator it_prefix, it_full;

  prefix = purge(prefix);
  full = purge(full);

  for(
      it_prefix = prefix.begin(), it_full = full.begin();
      it_prefix != prefix.end();
      it_prefix++, it_full++)
    ;

  for(; it_full != full.end(); it_full++)
    ret /= *it_full;

  return ret;
}
