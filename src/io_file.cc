/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#include"io_file.h"
#include"io_std.h"
#include"options.h"
#include"path.h"
#include"profile.h"

/*
 * Check information about path and its associated action is coherent.
 */
bool checkCoherent(fs::path p, action a) {
  // Does path exist?
  try{
    if (! fs::exists(p)) {
      if (!Options::global->GetQuiet())
        print_error << "srsync: Warning: path \"" << p << "\" does not exist."
          << endl;
      return false;
    }
  } catch(fs::filesystem_error& e) {
      if (!Options::global->GetQuiet())
        print_error << "srsync: Warning: Could not stat \"" << p
          << "\"." << endl;
    return false;
  }

  // Is path a non-directory with action 'descend'?
  if ((! fs::is_directory(p)) && a == a_descend) {
    if (!Options::global->GetQuiet())
      print_error << "srsync: Warning: Action 'descend' impossible on file "
        << p << "." << endl;
    return false;
  }

  return true;
}

/*
 * The three following functions extract information from a line of a config
 * file.
 * For all three functions, 'line' is a line from a config file, i.e. of the
 * form "a path" (where 'a' is a letter corresponding to an action, and
 * 'path' is a path).
 * checkLine returns true iff the line is well formed (see below).
 * readAction returns the action (precondition: checkLine returned true).
 * readPath returns the path (precondition: checkLine returned true).
 *
 * A well formed line (according to checkLine):
 * - begins with one of {a, y, n, d}
 * - has a space as second character
 * - is at least three characters long
 */
bool checkLine(string line) {
  if (line.length() < 3)
    return false;
  if (line[1] != ' ')
    return false;
  switch(line[0]) {
    case 'a':
    case 'y':
    case 'n':
    case 'd':
      break;
    default:
      return false;
  }
  return true;
}

action getAction(string line) {
  // Precondition: 'line' has at least one character
  switch(line[0]) {
    case 'a':
      return a_ask;
    case 'y':
      return a_accept;
    case 'n':
      return a_reject;
    case 'd':
      return a_descend;
    default:
      return a_none;
  }
}

fs::path getPath(string line) {
  // Precondition: 'line' has at least three characters
  return fs::path(line.substr(2));
}

/*
 * Read profile, and return corresponding Path structure
 */
path::Path* readProfile(string profile) {
  // Opening file
  fs::path file;
  file =
    fs::path(getenv("HOME")) / fs::path(".srsync/profiles") / fs::path(profile);
  if (! createDefault()) {
    return NULL;
  }
  if (!fs::exists(file)) {
    print_error << "srsync: Profile \"" << profile << "\" does not exist.\n";
    return NULL;
  }

  // Initialisation of Path structure
  path::Path* root = new path::Path("/", NULL, true);

  // Reading file
  fs::ifstream stream(file);
  string line;

  // Saving user option configuration, before replacing them by custom ones.
  Options::consider only = Options::global->GetOnly();
  Options::global->SetOnly(Options::consider_all);
  bool recursive = Options::global->GetRecursive();
  Options::global->SetRecursive(false);

  while (getline(stream, line)) {
    if(checkLine(line)) {
      list<string> path_l = path::pathToStrings(getPath(line));
      while(path_l.back() == ".")
        path_l.pop_back();

      if (checkCoherent(path::stringsToPath(path_l), getAction(line))) {
        // Path is coherent. We add it.
        root->addSub(path_l, getAction(line));
      } else {
        /*
         * Path is not coherent. In option "--purge" is on, ignore this path.
         * Else, add it anyway.
         */
        if (Options::global->GetPurge()) {
          if (!Options::global->GetQuiet())
            print_error << "Deleting path \"" << path::stringsToPath(path_l)
              << "\" from configuration file." << endl;
        } else {
          root->addSub(path_l, getAction(line));
        }
      }
      path_l.pop_front(); // We remove the leading '/'.
    } else {
      print_error
        << "srsync: Error while reading the following line of profile file \""
        << profile << "\"." << endl;
      print_error << "        " << line << endl;
    }
  }

  // Restoring user option configuration
  Options::global->SetOnly(only);
  Options::global->SetRecursive(recursive);
  
  return root;
}

/*
 * Write profile corresponding to "root".
 */
bool writeProfile(string profile, path::Path* root) {
  // Opening file
  fs::path file;
  file =
    fs::path(getenv("HOME")) / fs::path(".srsync/profiles") / fs::path(profile);
  if (! createDefault()) {
    return NULL;
  }
  if (!fs::exists(file)) {
    print_error << "srsync: Profile \"" << profile << "\" does not exist.\n";
    return NULL;
  }

  // Opening file
  fs::ofstream stream(file);

  // Iterating
  vector<string>::iterator pathList;
  path::Iterator it = root->Iteratorbegin();
  for(/* initialisation line before */; !it.over(); it++)
    switch((*it)->act) {
      case a_accept:
      case a_reject:
      case a_ask:
      case a_descend:
        stream << actionAbbr((*it)->act) << " " << path::purge((*it)->path).string()
          << endl;
      default:
    break;
    }

  return true;
}
