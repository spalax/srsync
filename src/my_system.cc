/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/wait.h>
#include <fcntl.h>

#include "my_system.h"
#include "io_std.h"

char** my_system::array_of_char_star(int count, ...) {
  int i;
  char** args = (char**) malloc(sizeof(char*) * (count + 1));
  char* token;
  va_list arguments;
  va_start(arguments, count);
  for(token = va_arg(arguments, char*), i = 0;
      i < count;
      ++i, token = va_arg(arguments, char*)){
    args[i] = token;
  }
  args[i] = (char*) 0;
  va_end(arguments);
  return args;
}

int my_system::safe_exec(
    char *command, char*arguments[], char* dir, char* output_file) {
  int pid;
  char* old_dir = get_current_dir_name();
  chdir(dir);
  pid = fork();
  if (pid == -1) { // Fork error
    free(arguments);
    return -1;
  } else if (pid == 0) { // Child
    if (output_file != NULL) {
      // Handling redirection
      int fd = open(output_file, O_WRONLY);
      if (fd == -1)
        exit(0);
      dup2(fd, STDOUT_FILENO);
      dup2(fd, STDERR_FILENO);
      close(fd);
    }
    execvp(command, arguments);
    free(arguments);
    exit(0);
  } else { // Parent
    free(arguments);
    waitpid(pid, NULL, 0);
  }
  chdir(old_dir);
  free(old_dir);
  return 0;
}
