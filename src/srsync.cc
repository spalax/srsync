/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>

#include "io_file.h"
#include "io_std.h"
#include "parser.h"
#include "path.h"
#include "profile.h"

/*
 * Global variable storing options chosen by user.
 */
Options::OptionsGlobal* Options::global;

/*******************************************************************************
 * Each of the following function corresponds to one (or several) subcommand.
 * They return the status code that is to be returned by the program.
 ******************************************************************************/
/*
 * Subcommand 'profile copy'
 * Return: status code to be returned by the program.
 */
int do_profile_copy() {
  if (profile_copy(
        Options::global->GetSrcProfile(),
        Options::global->GetDstProfile()))
    return 0;
  else
    return 1;
}

/*
 * Subcommand 'profile new'
 * Return: status code to be returned by the program.
 */
int do_profile_new() {
  if (profile_new(Options::global->GetProfile()))
    return 0;
  else
    return 1;
}

/*
 * Subcommand 'profile delete'
 * Return: status code to be returned by the program.
 */
int do_profile_delete() {
  if (profile_delete(Options::global->GetProfile()))
    return 0;
  else
    return 1;
}

/*
 * Subcommand 'profile list'
 * Return: status code to be returned by the program.
 */
int do_profile_list() {
  if (profile_list())
    return 0;
  else
    return 1;
}

/*
 * Subcommand 'profile' (profile manipulation)
 */
int do_profile() {
    switch(Options::global->GetProfileOperation()) {
      case subcommand_copy:
        return do_profile_copy();
      case subcommand_new:
        return do_profile_new();
      case subcommand_delete:
        return do_profile_delete();
      case subcommand_list:
        return do_profile_list();
      default:
        // Should not happen, but g++ complains.
        break;
    }
    // Should not happen, but g++ complains.
    throw Exception_General("Invalid profile option.");
    return 1;
}

/*
 * Subcommand 'status'
 * Return: status code to be returned by the program.
 */
int do_status() {
  // Reading profile
  path::Path* root;
  root = readProfile(Options::global->GetProfile());
  if (root == NULL)
    return 1;

  // Printing status
  vector<string>::iterator pathList;
  vector<string> paths = Options::global->GetPaths();
  list<string> absolute;
  for(pathList = paths.begin(); pathList != paths.end(); pathList++) {
    absolute = path::pathToStrings(path::purge(*pathList));
    if (fs::exists(*pathList)) {
      absolute.pop_front();
      if (Options::global->GetAbsolute())
        root->addSub(absolute, a_none);
      else
        root->addSub(absolute, a_none, *pathList)->PropagateName();
      path::StatusIterator it = root->statusIteratorbegin(absolute);
      for(/* initialisation line before */; ! it.over(); it++)
        if (Options::global->IsConsidered((*it).act, (*it).inherited))
          print_standard() << actionAbbr((*it).act) << " " << (*it).path
            << endl;
    } else {
      print_error << "srsync: Error: Path \"" << *pathList
        << "\" does not exist." << endl;
    }
  }
  // End
  delete root;
  return 0;
}

/*
 * Subcommand 'mark'
 * Return: status code to be returned by the program.
 */
int do_mark() {
  // Reading profile
  path::Path* root;
  path::Path* current;
  action to_set;
  vector<string>::iterator pathList;
  vector<string> paths;
  vector<string> new_p;
  list<string> absolute;
  bool change_action;

  root = readProfile(Options::global->GetProfile());
  if (root == NULL)
    return 1;

  // Changing status
  to_set = Options::global->GetMark();
  paths = Options::global->GetPaths();
  /*
   * Handling --recursive option:
   * If this option is set, we replace paths in the list by the list of
   * their subpaths.
   */
  if (Options::global->GetRecursive()) {
    new_p = vector<string>();
    for(pathList = paths.begin(); pathList != paths.end(); pathList++) {
      if (fs::exists(*pathList)) {
        new_p.push_back(*pathList);
        fs::recursive_directory_iterator itr;
        fs::recursive_directory_iterator end_itr; // End iterator
        for (itr = fs::recursive_directory_iterator(*pathList);
            itr != end_itr;
            ++itr )
          new_p.push_back(itr->path().string());
      }
    }
    paths = new_p;
  }

  /*
   * Here we go. For each path in the list, set its action.
   */
  for(pathList = paths.begin(); pathList != paths.end(); pathList++) {
    absolute = path::pathToStrings(path::purge(*pathList));
    if (fs::exists(*pathList) || Options::global->GetForce()) {
      absolute.pop_front();
      if (Options::global->GetAbsolute())
        current = root->addSub(absolute, a_none);
      else
        current = root->addSub(absolute, a_none, *pathList);
      /*
       * Setting new action, taking into account user options, and
       * coherence check
       */
      change_action = true;
      // --explicit and --marked
      if (change_action)
        if (! current->checkMarked())
          change_action = false;
      // No 'descend' on to_set non-directory
      if (change_action)
        if (to_set == a_descend && ! current->dir) {
          print_error << "srsync: Cannot set action '" << to_set
            << "' for directory \"" << current->path << "\"." << endl;
          change_action = false;
        }
      // Overwriting action? Options --force, --ignore
      if (change_action)
        if (current->act != a_none
            && to_set != a_none
            && current->act != to_set)
          if (! Options::global->GetForce())
            if (! io_std::askOverwriteAction(
                  current->path, current->act, to_set))
              change_action = false;
      // Now we can change the action, if relevant.
      if (change_action)
        current->act = to_set;
    } else {
      print_error << "srsync: Error: Path \"" << *pathList
        << "\" does not exist." << endl;
    }
  }

  // Writing back profile
  writeProfile(Options::global->GetProfile(), root);

  // End
  delete root;
  return 0;
}

/*
 * Subcommands 'exec', 'print'
 * Return: status code to be returned by the program.
 */
int do_exec() {
  path::Path *root, *current;
  vector<string>::iterator pathList;
  list<struct path::statusData> acceptedList;
  vector<string> paths;
  vector<string> new_p;
  list<string> absolute;
  std::ostream* output = &print_null;
  ostringstream command;
  map<string,list<struct path::statusData> > dirs;
  map<string,string> commandList;
  bool valid_paths;
  int to_return = 0;

  // Reading profile
  root = readProfile(Options::global->GetProfile());
  if (root == NULL)
    return 1;

  // Setting output
  if (Options::global->cl_subcommand == subcommand_print) {
    switch(Options::global->GetOutput()){
      case type_print_list:
      case type_print_nlist:
        output = &print_standard();
        break;
      case type_print_none:
        output = &print_null;
        break;
    }
  } else if (Options::global->cl_subcommand == subcommand_exec) {
    if (Options::global->GetCommand() == type_exec_print)
      set_standard_output(error);
    command << Options::global->GetBin() << " ";
    // Adding default options to command
    command << "-r ";
    // Adding user rsync options to command
    vector<string> options = Options::global->GetRsyncOptions();
    for(vector<string>::iterator it = options.begin();
        it != options.end();
        it++)
      command << *it << " ";
  }

  paths = Options::global->GetPaths();
  // Check that there is at least one source path for --exec option
  if (Options::global->cl_subcommand == subcommand_exec && paths.empty()) {
    print_error << "srsync: Error: No source path given." << endl;
    return 1;
  }
  // Does paths given by user exist?
  valid_paths = true;
  for(pathList = paths.begin(); pathList != paths.end(); pathList++) {
    absolute = path::pathToStrings(path::purge(*pathList));
    if (! fs::exists(*pathList)) {
      print_error << "srsync: Error: File \"" << *pathList
        << "\" does not exist." << endl;
      valid_paths = false;
    }
    if (Options::global->cl_subcommand == subcommand_exec)
      if (! path::subpath(Options::global->GetOrigin(), *pathList)) {
        print_error << "srsync: Error: Path \""
          << Options::global->GetOrigin() << "\" is not a prefix of \""
          << *pathList << "\"." << endl;
        valid_paths = false;
      }
  }
  if (! valid_paths)
    return 1;

  // Adding paths set by user in option, and iteration
  for(pathList = paths.begin(); pathList != paths.end(); pathList++) {
    absolute = path::pathToStrings(path::purge(*pathList));
    absolute.pop_front();
    if (Options::global->GetAbsolute()) { 
      current = root->addSub(absolute, a_none);
    } else {
      current = root->addSub(absolute, a_none, *pathList);
      current->PropagateName();
    }
    acceptedList = current->AcceptedList();
    list<struct path::statusData>::iterator it;
    if (Options::global->cl_subcommand == subcommand_print)
      for(it = acceptedList.begin(); it != acceptedList.end(); it++)
        *output << (*it).path.string() << endl;
    else if (Options::global->cl_subcommand == subcommand_exec)
      dirs[*pathList] = acceptedList;
  }

  // Building command list
  if (Options::global->cl_subcommand == subcommand_exec) {
    if (Options::global->GetOrigin() == "") {
      commandList[""] = command.str();
      for(map<string,list<struct path::statusData> >::iterator
          it = dirs.begin();
          it != dirs.end();
          it++) {
        for(list<struct path::statusData>::iterator
            p_it = dirs[it->first].begin();
            p_it != dirs[it->first].end();
            p_it++) {
          if ((*p_it).act == a_accept) {
            commandList[""] += "--filter=\"+/ "
              + (*p_it).path.string()
              + "\" ";
          } else {
            commandList[""] += "--filter=\"-/ "
              + (*p_it).path.string()
              + "\" ";
          }
        }
      }
      for(map<string,list<struct path::statusData> >::iterator
          it = dirs.begin();
          it != dirs.end();
          it++)
        commandList[""] += it->first + " ";
      commandList[""] += Options::global->GetDest();
    } else {
      for(map<string,list<struct path::statusData> >::iterator
          it = dirs.begin();
          it != dirs.end();
          it++) {
        commandList[it->first] = command.str();
        for(list<struct path::statusData>::iterator
            p_it = dirs[it->first].begin();
            p_it != dirs[it->first].end();
            p_it++) {
          if ((*p_it).act == a_accept) {
            commandList[it->first] += "--filter=\"+/ " + path::remove_prefix(
                fs::path(Options::global->GetOrigin()),
                (*p_it).path).string()
              + "\" ";
          } else {
            commandList[it->first] += "--filter=\"-/ " + path::remove_prefix(
                fs::path(Options::global->GetOrigin()),
                (*p_it).path).string()
              + "\" ";
          }
        }
        commandList[it->first] += it->first + " "
          + (fs::path(Options::global->GetDest())
              / path::remove_prefix(Options::global->GetOrigin(),
                it->first)).remove_filename().string();
      }
    }
  }


  // If relevant, executing command
  bool do_exec = true;
  if (Options::global->cl_subcommand == subcommand_exec) {
    if (Options::global->GetCommand() == type_exec_print)
      set_standard_output(standard);
    if (Options::global->GetCareful()
        || Options::global->GetCommand() == type_exec_print)
      for(map<string,string>::iterator it = commandList.begin();
          it != commandList.end();
          it++)
        print_standard() << it->second << endl;

    if (Options::global->GetCareful())
      do_exec = io_std::askConfirmExecute();
    else
      do_exec =
        (Options::global->GetCommand() == Options::type_exec_rsync);

    if (do_exec) {
      int returnValue;
      for(map<string,string>::iterator it = commandList.begin();
          it != commandList.end();
          it++) {
        returnValue = system(it->second.c_str());
        if (returnValue != 0)
          to_return = 2;
      }

    }

  }

  // Writing back profile
  if (! Options::global->GetOneShot())
    writeProfile(Options::global->GetProfile(), root);

  // End
  delete root;
  return to_return;
}

/*******************************************************************************
 * Main
 ******************************************************************************/
int main(int argc, char* argv[]) {
  Parser parser;

  // Parsing command line.
  Options::global = parser.parse(argc, argv);

  if (Options::global == NULL)
    // Errors where found in command line. No need to go further.
    return 1;

  /*
   * 'Options::global' now contain the information provided by the user in the
   * command line.
   */

  int status_code = 0; // Status code to return at the end of the program

  switch(Options::global->cl_subcommand) {
    case subcommand_help:
    case subcommand_version:
      // This case has already been treated while parsing.
      status_code = 0;
      break;

    case subcommand_profile:
      status_code = do_profile();
      break;

    case subcommand_status:
      status_code = do_status();
      break;

    case subcommand_mark:
      status_code = do_mark();
      break;

    case subcommand_exec:
    case subcommand_print:
      status_code = do_exec();
      break;

    case subcommand_none:
      print_error
        << "srsync: This line should not be printed. Please fill a bug report."
        << endl;
      status_code = 1;
  }

  delete Options::global;
  return status_code;
}
