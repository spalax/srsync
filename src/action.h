/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ACTIONS_H__
#define __ACTIONS_H__

#include<ostream>
using namespace std;

/*
 * List of actions that can be set on a path.
 */
enum action {
  a_accept, // Accept the path
  a_ask, // Always ask user what to do
  a_descend, // Descend in the directory, and study each of its children
  a_forget, // Forget the mark put on the path
  a_reject, // Reject the path
  a_none // No action
};

/*
 * Definition of << for variables of type 'action'.
 */
template <class charT, class Traits> basic_ostream<charT, Traits> &operator<<
  (basic_ostream<charT, Traits> &stream, const action &a) {
  typename basic_ostream<charT, Traits>::sentry init(stream);
  if (init) {
    switch(a) {
      case a_accept:
        stream << "accept";
        break;
      case a_ask:
        stream << "ask";
        break;
      case a_descend:
        stream << "descend";
        break;
      case a_forget:
        stream << "forget";
        break;
      case a_reject:
        stream << "reject";
        break;
      case a_none:
        stream << "none";
        break;
    }
  }
  return stream;
}

// Return the abbreviation corresponding to an action
char actionAbbr(action a);

#endif
