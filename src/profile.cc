/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_FILESYSTEM_VERSION 3
#include<boost/filesystem.hpp>
#include<boost/filesystem/fstream.hpp>
namespace fs = boost::filesystem;

#include"profile.h"
#include"io_std.h"

/*
 * This function checks if ~/.srsync exists, and create it if it does not.
 * Return 'false' if ~/.srsync still does not exist when function returns.
 */
bool createDotSrsync() {
  fs::path _srsync = fs::path(getenv("HOME")) / fs::path(".srsync");
  if (fs::exists(_srsync))
   if (fs::is_directory(_srsync)) {
    // Directory already exists. Nothing to do.
    return true;
  }
  if (fs::exists(_srsync))
    if (!fs::is_directory(_srsync)) {
      // Path exist but is not a directory. Error.
      print_error << "srsync: Cannot create directory \"" << _srsync << "\".\n";
      return false;
    }

  try{
    fs::create_directory(_srsync);
  }
  catch(...){
    print_error << "srsync: Cannot create directory \"" << _srsync << "\".\n";
    return false;
  }
  return true;
}

/*
 * This function checks if ~/.srsync/profiles exists, and create it if it does
 * not.
 * Return 'false' if ~/.srsync/profiles still does not exist when function
 * returns.
 */
bool createProfilesFolder() {
  // Ensuring ~/.srsync exists
  if (! createDotSrsync())
    return false;

  fs::path profilesFolder;
  profilesFolder = fs::path(getenv("HOME")) / fs::path(".srsync/profiles");
  if (fs::exists(profilesFolder))
    if (fs::is_directory(profilesFolder))
      // Directory already exists. Nothing to do.
      return true;

  if (fs::exists(profilesFolder))
    if (!fs::is_directory(profilesFolder)) {
      // Path exist but is not a directory. Error.
      print_error
        << "srsync: Cannot create directory \"" << profilesFolder << "\".\n";
      return false;
    }

  try{
    fs::create_directory(profilesFolder);
  }
  catch(...){
    print_error
      << "srsync: Cannot create directory \"" << profilesFolder << "\".\n";
    return false;
  }
  return true;
}

/*
 * This function checks if file ~/.srsync/profiles/default exists. If not,
 * create an empty file.
 * Return false if file ~/.srsync/profiles/default does not exist when function
 * returns.
 */
bool createDefault() {
  // ~/.srsync/profiles must exist
  if (!createProfilesFolder())
    return false;
  fs::path default_path =
    getenv("HOME") / fs::path(".srsync/profiles") / fs::path("default");
  if (fs::exists(default_path) && fs::is_regular_file(default_path)) {
    // File already exist. Nothing to do
    return true;
  }
  if (fs::exists(default_path) && !fs::is_regular_file(default_path)) {
    // Path exists but is not a regular file.
    print_error << "srsync: File \"" << default_path
      << "\" is not a regular file.\n";
    return false;
  }
  try{
    fs::ofstream default_file(default_path);
    default_file.close();
  }
  catch(...){
    print_error << "srsync: "
      << "Error while creating empty profile \"default\". Is directory \""
      << getenv("HOME") / fs::path(".srsync/profiles") << "\" writable?\n";
  }
  return true;
}

bool profile_copy(string src, string dest) {
  // ~/.srsync/profiles must exist
  if (!createProfilesFolder())
    return false;
  
  /*
   * If source file is default, we must ensure it exists (and create it is
   * necessary). If not, we must check if it exists, and return an error if
   * not.
   */
  fs::path p_src;
  if (src == "default") {
    if (!createDefault())
      return false;
    p_src =
      getenv("HOME") / fs::path(".srsync/profiles") / fs::path("default");
  } else {
    // ~/.srsync/profiles/$(src) must exist, and be a regular file.
    p_src =
      fs::path(getenv("HOME")) / fs::path(".srsync/profiles") / fs::path(src);
    if (!fs::exists(p_src)) {
      print_error << "srsync: Profile \"" << src << "\" does not exist.\n";
      return false;
    }
  }

  // ~/.srsync/profiles/$(dest) must not exist
  fs::path p_dest
    = fs::path(getenv("HOME")) / fs::path(".srsync/profiles") / fs::path(dest);
  if (fs::exists(p_dest)) {
    print_error << "srsync: Profile \"" << dest << "\" already exists.\n";
    return false;
  }

  // Now we can copy
  try{
    fs::copy_file(p_src, p_dest);
  }
  catch(...) {
    print_error << "srsync: Error while copying profile \"" << src << "\" to \""
      << dest << "\". Are you sure file \"" << p_src << "\" is readable?.\n";
    return false;
  }
  return true;
}

bool profile_new(string name) {
  // ~/.srsync/profiles must exist
  if (!createProfilesFolder())
    return false;

  // Special case: profile "default"
  if (name == "default")
    if (!createDefault())
      return false;

  // Does profile already exist?
  fs::path p_new
    = fs::path(getenv("HOME")) / fs::path(".srsync/profiles") / fs::path(name);
  if (fs::exists(p_new)) {
    print_error << "srsync: Profile \"" << name << "\" already exist.\n";
    return false;
  }

  // Creating empty file
  try{
    fs::ofstream f_new(p_new);
    f_new.close();
  }
  catch(...){
    print_error << "srsync: Error while creating empty profile \"" << name
      << "\". Is directory \"" << getenv("HOME") / fs::path(".srsync/profiles")
      << "\" writable?\n";
  }
  return true;
}

bool profile_delete(string name) {
  // ~/.srsync/profiles must exist
  if (!createProfilesFolder())
    return false;

  // Check if not deleting 'default'.
  fs::path p_file;
  if (name == "default") {
    print_standard() << "Ready to delete default profile. Are you sure? [y/n] ";
    char list[] = {'y', 'n'};
    if (io_std::choose_char(2, list, 'n') == 'n')
      return true;
  }
  p_file =
    fs::path(getenv("HOME")) / fs::path(".srsync/profiles") / fs::path(name);

  if (!fs::exists(p_file)) {
    // Profile does not exist
    print_error << "srsync: Impossible to delete non-existent profile \""
      << name << "\".\n";
    return false;
  }

  try{
    fs::remove(p_file);
  }
  catch(...) {
    print_error << "srsync: Error while deleting profile \"" << name
     << "\". Is ~/.srsync/profiles writable?\n";
  }

  return true;
}

bool profile_list() {
  // ~/.srsync/profiles must exist
  if (!createProfilesFolder())
    return false;

  // Print regular files contained in ~/.srsync/profiles
  fs::path profilesFolder;
  profilesFolder = fs::path(getenv("HOME")) / fs::path(".srsync/profiles");
  fs::directory_iterator end;
  for (fs::directory_iterator it(profilesFolder); it != end; ++it )
    if (fs::is_regular_file(it->status()))
      print_standard() << it->path().stem().string() << "\n";

  return true;
}
