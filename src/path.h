/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file contain the definition of class Path, which store information
 * associated to a path (which action to perform on it, etc.).
 */

#ifndef __PATH_H__
#define __PATH_H__

#include<map>
#include<string>
#include<list>
#define BOOST_FILESYSTEM_VERSION 3
#include<boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include"global.h"
#include"action.h"



namespace path {
  class Path;

  /*
   * This struct hold information about the action associated with a path
   */
  struct statusData {
    fs::path path; // Path
    action act; // Action associated with it
    bool inherited; // false if path is explicitly marked with the action,
                    // true if it inherits it.
  };

  /*****************************************************************************
   * Iterator
   ****************************************************************************/
  /*
   * Iterator over an object "path" and its subpaths.
   * This iterator does not iterate over real paths, but over paths that are
   * stored as a 'Path' object.
   */
  class Iterator {
    public:
      // Constructor
      Iterator(Path *p);

      // Destructor
      ~Iterator();

      // Content of iterator
      Path* operator*();

      // Incrementation
      Iterator operator++(int);

      // Return true if iteration is finished
      bool over() const;

    private:
      /*
       * Current path.
       */
      path::Path *current_path;

      /*
       * End mark. Is true iff everything has been iterated.
       */
      bool endMark;

      /*
       * Recursive iterator: iterator on subpaths of this->children.
       * If recursive==NULL, this means that current path has not been treated
       * yet.
       */
      Iterator* recursive;

      /*
       * Iterator over children of current path
       */
      map<string, Path*>::iterator children;
  };

  /*****************************************************************************
   * Status iterator
   ****************************************************************************/
  /*
   * This iterator iterates over all subpath of an object 'Path': both real
   * paths and path represented as a 'Path' objects.
   */
  class StatusIterator {
    public:
      // Constructor
      StatusIterator(Path *p, action a);
      StatusIterator(fs::path p, action a);

      // Destructor
      ~StatusIterator();

      // Content of iterator
      struct statusData operator*();

      // Incrementation
      StatusIterator operator++(int);

      // Return true if iteration has finished
      bool over() const;

    private:
      /*
       * Iterator on the directory this->path (if this->path is a directory).
       *
       * If (this->children == directory_iterator end_itr), the next element to
       * return is the path corresponding to 'this->path'. Else, it is the
       * path corresponding to '*this->recursive'.
       */
      fs::directory_iterator children;

      /*
       * If stupid==true, this means that we are exploring a tree of Path
       * objects.
       * If stupid==false, this means that are exploring the file system
       * tree, knowing that all path have the same action.
       */
      bool stupid;

      /*
       * Current path.
       * custom_path is relevant iff stupid == false
       * fs_path is relevant iff stupid == true
       */
      path::Path *custom_path;
      fs::path fs_path;

      /*
       * Current inherited action
       */
      action act;

      /*
       * End mark. Is true iff everything has been iterated.
       */
      bool endMark;

      /*
       * Recursive status iterator: iterator on subpaths of this->children.
       */
      StatusIterator* recursive;
  };



  /*****************************************************************************
   * Class Path
   ****************************************************************************/
  class Path {
    friend class StatusIterator;
    friend class Iterator;

    public:
      // Contructor
      Path(
          fs::path p,
          Path* parent,
          bool dir,
          action act = a_none,
          action inherited = a_none);

      // Destructor
      virtual ~Path();

      /*
       * 'l' is a list of directories (maybe ending with a file),
       * representing a path, relative to this->path.
       * For instance, if this->path = "/home/user" and l = ["work",
       * "mail"], considered path is "/home/user/work/mail".
       *
       * If path is in this path structure, add it with action 'a',
       * creating new Path children.
       * Check that path exists, and that action is coherent (no 'descend' on a
       * file, for instance).
       * Return NULL if path could not be added, for any reason.
       * Return the created object otherwise.
       *
       * 'user' is the relative path corresponding to 'l', as written by the
       * user in the command line (if relevant).
       */
      Path* addSub(list<string> l, action a, fs::path user = fs::path());

      /*
       * Precondition: this is a directory.
       * Build object corresponding to subpaths of this, with action 'a'.
       */
      void buildAllSub(action a = a_none);

      /*
       * Return the action of 'this', or the inherited one if action is
       * 'a_none'.
       * If 'res' is true, restrict the returned value to the one that
       * can be inherited, that is {accept, reject, nothing}. If any other
       * value than {accept reject} would have been returned, return a_none.
       */
      action actualAction(bool res);

      /*
       * Return true iff the relative path represented by the argument is
       * considered (according to options --marked and --explicit).
       */
      bool checkMarked(list<string> p = list<string>());

      /*
       * Return an iterator over this object and its children.
       */
      Iterator Iteratorbegin();

      /*
       * Return a list of 'struct statusData' which is the list of paths to
       * deal with.
       * This iterator asks user what to do when meeting unmarked paths, or
       * paths marked with 'ask', or wathever.
       */
       list<struct statusData> AcceptedList();

      /*
       * Return an iterator over the list of paths, with their status (the
       * action associated to them).
       */
      StatusIterator statusIteratorbegin(list<string> p = list<string>());

      /*
       * Return true iff all subpaths of this->path inherit (or seem to
       * inherit) their action from this path.
       * By "seems to inherit", we mean that the subpath is explicitly marked
       * for the action it would have inherited anyway.
       */
      bool AllImplicit();

      /*
       * Update children of current object, to make their path relative to the
       * path of this object.
       */
      void PropagateName();

      /*
       * Update children of current object, to make their inherited action
       * coherent with 'this->act'.
       */
      void PropagateAction();

      /*
       * Print the tree of paths stored in "this".
       * Debug only
       *
       * Argument: depth of current path, relative to initial call of printTree.
       */
      void printTree(int depth = 0);

      // Path (absolute, or relative to command line arguments) of the object
      fs::path path;

      // Action to perform on this path
      // Condition: 'act' is one of {a_none, a_accept, a_reject, a_descend,
      // a_ask}
      action act;

      // True iff path is a directory
      // A directory here is anything that can have subfiles (real directory,
      // symbolic link to a directory, directory on another partition, etc.)
      bool dir;

    private:
      /*
       * Return the list of paths to deal with, and set 'act' to be the action
       * performed on every subpath of this->path, that is:
       * - a_accept if all subpaths are accepted (and recursively)
       * - a_reject if all subpaths are rejected (and recursively)
       * - a_none if none of the above (some subpaths are accepted, some are
       *   rejected)
       */
      list<struct statusData> AcceptedSubpathList(action &act);

      // List of children (if this path is a directory; irrelevant otherwise)
      map<string, Path*> children;

      // Action inherited from parent path
      // Condition: 'inherited_action' is one of {a_none, a_accept, a_reject}
      action inherited_action;

      // Parent of current path
      // If path is root, this->parent == this
      Path* parent;
  };

  /*****************************************************************************
  * Function
  *****************************************************************************/
  /*
   * Convert a path to a list of strings.
   * Argument: a path
   * Return: a list of strings
   *
   * Postcondition: 'p' being the argument, and 'v' the returned value, 'p' and
   * the following object represent the same path.
   * boost::filesystem::path(v[0] / v[1] / ... / v[v.length()])
   */
  list<string> pathToStrings(fs::path p);

  /*
   * Perform the operation converse to the previous one
   */
  fs::path stringsToPath(list<string> s);

  /*
   * Return a path, which represents the same path as the argument, excepted
   * that it does not have any "." directories (excepted maybe the leading
   * one).
   * E.g. purge(./some/path/./with/././useless/./dots/.) returns
   * ./some/path/with/useless/dots
   */
  fs::path purge(fs::path p);

  /*
   * Given an iterator map<string, Path*>::iterator over the children of a
   * Path, iterate it (if necessary) to skip "." and ".." children.
   */
  map<string, Path*>::iterator skipDots(
      map<string, Path*>::iterator &it, map<string, Path*>::iterator end);

  /*
   * Return true iff 'sub' is a subpath of 'p' (i.e. iff there exists a path
   * 'q' such that "p = sub/q").
   * Convention: for any 'p', subpath("", p) returns true.
   */
  bool subpath(string sub, string p);

  /*
   * Returns path 'full', without its prefix 'prefix'.
   * Precondition: subpath(prefix, full) == true
   */
  fs::path remove_prefix(fs::path prefix, fs::path full);
}
#endif
