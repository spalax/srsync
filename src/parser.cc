/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

// Print to console
#include<iostream>

// Packages needed to parse the command line
#include<boost/program_options/options_description.hpp>
#include<boost/program_options/parsers.hpp>
#include<boost/program_options/variables_map.hpp>
#include<boost/program_options/errors.hpp>
#include<boost/tokenizer.hpp>
#include<boost/token_functions.hpp>
using namespace boost;
using namespace boost::program_options;

// Other packages from srsync
#include"io_std.h"
#include"parser.h"
#include"version.h"

/*******************************************************************************
 * Some exceptions
 ******************************************************************************/
class Exception_SeveralSubcommands : public Exception_General {
  public:
    // Constructor. Argument: subcommand given by user.
    Exception_SeveralSubcommands(string sub, set<string> match) {
      this->sub = sub;
      this->match = match;
    }

    const string what() throw() {
      ostringstream cause;
      set<string>::iterator it;
      cause << "String '" << this->sub << "' matches several subcommands:";
      for(it = this->match.begin(); it != this->match.end(); it++)
       cause << " '" << *it << "'";
      cause << endl;
      return cause.str();
    }

    ~Exception_SeveralSubcommands() throw() {
    }

  private:
    string sub;
    set<string> match;
};

/*******************************************************************************
 * Miscallenous functions
 ******************************************************************************/

string extractQuoted(int n, char c, string s) {
  /*
   * Extract the option name from an error message given by a
   * boost::program_option exception.
   */
  int index;
  string::size_type begin, end;
  begin = string::npos;

  for(index = n * 2 + 1; index > 0; index--) {
    if (begin == string::npos)
      // Handling first search: search begin at index 0; not "begin+1"
      begin = 0;
    begin = s.find_first_of(c, begin + 1);
    if (begin == string::npos)
      return "";
  }
  end = s.find_first_of(c, begin + 1);
  if (end == string::npos)
    return "";
  return s.substr(begin + 1, end - begin - 1);
}

template <class return_type> return_type findCommand(
    string p, set<pair<string, return_type> > s, return_type def) {
  /*
   * Find which to which command corresponds the argument, and return the
   * corresponding value. The argument can be the exact string, of the prefix
   * on any command.
   *
   * Arguments:
   * - p: the string that is supposed to be the prefix of some command;
   * - s: a set of available commands. First item of pair is the string to
   *   match against 'p', the second item is the object to return if the first
   *   item match.
   * - def: the default value to return if no match is found.
   *
   * Return:
   *  The corresponding return values (of type "return_type") of the only pair
   *  of "s" whose firt item has "p" for prefix.
   *
   * Throw:
   *    Exception_SeveralSubcommands if several match are found.
   *
   * Example:
   *  The set being: pairs = {("one", 1), ("two", 2), ("three", 3)}
   *  findCommand<int>("tw", pairs, 0) will return 2.
   *  findCommand<int>("t", pairs, 0) will throw an exception (for "t" is both
   *    a prefix of "two" and "three").
   *  findCommand<int>("X", pairs, 0) will return 0 (default value, for no
   *    match was found).
   */
  set<return_type> match;
  set<string> string_match;
  typename set<pair<string, return_type> >::iterator it;
  for(it = s.begin(); it != s.end(); it++) {
    if (it->first.find(p) == 0) {
      string_match.insert(it->first);
      match.insert(it->second);
    }
  }
  switch(match.size()) {
    case 0:
      return def;
    case 1:
      return *(match.begin());
    default:
      throw Exception_SeveralSubcommands(p, string_match);
  }
}

pair<string, string> swallow_all(const string& s) {
  // Function to be used by boost program_options as an extra parser.
  // Consider all options but version and help as positional arguments (assign
  //  them to paths option).
  if ((s.compare("--version") == 0)
      || (s.compare("--help") == 0)
      || (s.compare("-h") == 0)) {
    return make_pair(string(), string());
  } else {
    return make_pair("paths", s);
  }
}

/*******************************************************************************
 * Parsers for subcommands
 ******************************************************************************/
options_description options_update() {
  // Return the "options_description" object that describes options common to
  // every commands that update configuration, that is: mark, status, print and
  // exec.
  options_description general("Update options");
  general.add_options()
    ("help,h", "Print help and exit.")
    ("absolute,b", "refer to paths by their absolute name")
    ("quiet,q",  "do not print warnings about incoherent configuration")
    ("purge,p", "forget references to non-existent paths.")
    ("profile,P", value<string>()->default_value("default"), "use profile "
     "'arg' (default is \"default\").")
    ;
  return general;
}

void add_search_accepted(options_description &options) {
  // Add to 'options' the options common to every subcommand that search for
  // accepted paths, that is, for subcommands exec and print.
  options.add_options()
    ("no-ask", value<char>()->implicit_value('n'), "do not ask what to do "
       "with not-handled paths. If an optional argument is set, apply its "
       "action (default is 'n').\n\ty accept path\n\tn reject path")
    ("one-shot,1", "do not save changes")
    ("shell-output", value<string>()->default_value("default"), "set the "
       "output of a shell that might be invoked while running srsync. This "
       "may be useful when output of srsync is stored in a variable, or "
       "passed by a pipe to another command. Available values are:\n"
       "\tnone: shell output is discarded;\n"
       "\tstdout: shell output is printed on standard output;\n"
       "\tstderr: shell output is printed on standard error;\n"
       "\tdefault: shell output is printed on default output."
     )
    ;
}

void process_options_update(variables_map vm, OptionsGlobal* options) {
  /*
   * Process options described in "options_update".
   *
   * Arguments:
   * - vm: boost object holding options information after parsing
   * - options: where to store option information.
   */
  if (vm.count("purge"))
    options->SetPurge(true);
  if (vm.count("quiet"))
    options->SetQuiet(true);
  if (vm.count("absolute"))
    options->SetAbsolute(true);
  if (vm.count("profile")) {
    options->SetProfile(vm["profile"].as<string>());
  }
}

void process_options_search_accepted(variables_map vm, OptionsGlobal* options) {
  /*
   * Process options described in "add_search_accepted".
   *
   * Arguments:
   * - vm: boost object holding options information after parsing
   * - options: where to store option information.
   */
   if (vm.count("one-shot")) {
     options->SetOneShot(true);
   }
   if (vm.count("no-ask")) {
     switch(vm["no-ask"].as<char>()) {
       case 'y':
         options->SetNoAsk(a_accept);
         break;
       case 'n':
         options->SetNoAsk(a_reject);
         break;
       default:
         vector<string> accepted;
         string list[] = {"y", "n"};
         accepted.insert(accepted.begin(), list, list + 2);
         throw Exception_WrongSet("no-ask", string(1,vm["no-ask"].as<char>()),
             accepted, false);
     }
   }
   if (vm.count("shell-output")) {
     string choice = vm["shell-output"].as<string>();
     vector<string> accepted;
     string list[] = {"stdout", "stderr", "none", "default"};
     accepted.insert(accepted.begin(), list, list + 4);
     vector<string>::iterator it;
     it = find(accepted.begin(), accepted.end(), choice);
     if (it == accepted.end()) {
       throw Exception_WrongSet("shell-output",
           vm["shell-output"].as<string>(), accepted, false);
     } else {
      options->SetShellOutput(choice);
     }
   }
}

OptionsGlobal* parse_none(int argc, char* argv[]) {
  // Parsing when no subcommand is given.
  OptionsGlobal* options;

  options_description hidden("hidden options");
  hidden.add_options()
    ("paths", value<vector<string> >())
    ;
  options_description general("General options");
  general.add_options()
    ("help,h", "Print help and exit.")
    ("version", "Output the version number.")
    ;
  options_description all("");
  options_description visible("");
  visible.add(general);
  all.add(general).add(hidden);

  /*
   * Parsing
   */
  variables_map vm;
  store(command_line_parser(argc, argv).
      options(all).extra_parser(swallow_all).run(), vm);
  notify(vm);

  /*
   * Analysing result
   */
  if (vm.count("version")) {
    print_version();
    options = new OptionsHelpVersion();
    // We do not want any further option to be analysed.
    return options;
  }

  if (vm.count("help") || argc == 1) {
    print_standard()
      << "Usage: srsync [--help/-h] [--version] COMMAND" << endl
      << visible 
      << endl
      << "Available commands (use \"srsync COMMAND --help\" for more "
         "information.):" << endl
      << "  exec   \t\tUpdate configuration and synchronise files." << endl
      << "  rsync  \t\tShortcut for \"exec rsync\"." << endl
      << "  mark   \t\tMark (or forget mark about) files." << endl
      << "  print  \t\tUpdate configuration and print the list of files that "
         "would be synchronised." << endl
      << "  profile\t\tManage profiles." << endl
      << "  status \t\tPrint status (mark set on) given files." << endl
      ;
    if (vm.count("help")) {
      options = new OptionsHelpVersion();
      return options;
    } else {
      return NULL;
    }
  }

  if (argv[1][0] == '-')
    print_error << "srsync: Unknown option \"" << argv[1] << "\"." << endl;
  else
    print_error << "srsync: \"" << argv[1] << "\" is not a srsync command."
      << endl;
  return NULL;
}

OptionsGlobal* parse_print(int argc, char* argv[]) {
  // Parsing when subcommand print is given.
  /*
   * Handling subcommand
   */
  set<pair<string, type_print> > all_actions;
  { // Initialisation of all_actions
    all_actions.insert(make_pair("list", type_print_list));
    all_actions.insert(make_pair("nlist", type_print_nlist));
    all_actions.insert(make_pair("none", type_print_none));
  }
  type_print act;

  if (argc <= 1) {
    act = type_print_list;
  } else {
    try {
      act = findCommand<type_print>(
          argv[1], all_actions, type_print_list);
    } catch (Exception_SeveralSubcommands& e) {
      print_error << "srsync: " << e.what() << endl;
      return NULL;
    }
    if (act == type_print_list
        && string("list").find(argv[1]) == string::npos) {
      // Default is "list", but in this case, we do not remove the first item
      // of the options: it was not the subcommand
      ;
    } else {
      argc --;
      argv ++;
    }
  }

  /*
   * Parsing
   */
  OptionsGlobal* options = NULL;
  options_description hidden("hidden options");
  hidden.add_options()
    ("paths", value<vector<string> >())
    ;
  positional_options_description paths;
  paths.add("paths", -1);

  options_description general = options_update();
  add_search_accepted(general);

  options_description all("");
  options_description visible("");
  visible.add(general);
  all.add(general).add(hidden);

  variables_map vm;
  store(command_line_parser(argc, argv).
      options(all).positional(paths).run(), vm);
  notify(vm);

  /*
   * Analysing result, and executing commands.
   */
  if (vm.count("help")) {
    print_standard()
      << "Usage: srsync print [--help] ACTION ARGUMENTS" << endl
      << "Update configuration and print a set of files, according to action."
        << endl
      << endl
      << "The following actions are available:" << endl
      << "  list \tPrint the list of files that would be synchronised." << endl
      << "  nlist\tPrint the list of files that would not be synchronised."
        << endl
      << "  none \tDo not print anything (update only)." << endl
      << endl
      << visible
      ;
    options = new OptionsHelpVersion();
    return options;
  }

  options = new OptionsPrint(act);
  process_options_update(vm, options);
  process_options_search_accepted(vm, options);

  if (vm.count("paths"))
    options->SetPaths(vm["paths"].as<vector<string> >());

  return options;
}

OptionsGlobal* parse_status(int argc, char* argv[]) {
  // Parsing when subcommand status is given.
  OptionsGlobal* options;

  options_description hidden("hidden options");
  hidden.add_options()
    ("paths", value<vector<string> >())
    ;
  positional_options_description paths;
  paths.add("paths", -1);

  options_description general = options_update();
  general.add_options()
    ("explicit,e", value<string>()->implicit_value("ynad"), "only consider "
      "files that are explicitly marked. If an optional argument is given, "
      "only consider files that are marked as specified by this argument: "
      "respectively y, n, d, a, x for paths marked as accepted, rejected, "
      "descend, ask, not marked.\nCombinations are allowed, e.g. \"-e yx\" "
      "will only consider paths that are not marked, or marked as accepted.")
    ("marked,m", value<string>()->implicit_value("ynad"), "only consider "
      "files that are explicitly or implicitely marked. If an optional "
      "argument is given, only consider files that are marked as specified "
      "by this argument: respectively y, n, d, a, x for paths marked as "
      "accepted, rejected, descend, ask, not marked.\nCombinations are "
      "allowed, e.g. \"-m yx\" will only consider paths that are not marked, "
      "or marked as accepted.")
    ("useful,u",  "with --status and -r, only print useful paths: do not "
      "print subpaths of a directory in which every subpath inherits its "
      "status from its parent")
    ("recursive,r",  "recursively perform modes on paths.")
    ;
  options_description all("");
  options_description visible("");
  visible.add(general);
  all.add(general).add(hidden);

  /*
   * Parsing
   */
  variables_map vm;
  store(command_line_parser(argc, argv).
      options(all).positional(paths).run(), vm);
  notify(vm);

  /*
   * Analysing result
   */
  options = new OptionsStatus();
  process_options_update(vm, options);
  if (vm.count("help")) {
    print_standard()
      << "Usage: srsync status [OPTIONS] PATHS" << endl
      << "Print status of paths given in argument." << endl
      << visible 
      ;
    options = new OptionsHelpVersion();
    return options;
  }
  if (vm.count("recursive"))
    options->SetRecursive(true);
  if (vm.count("useful")) {
    options->SetUseful(true);
    if (! options->GetRecursive())
      throw Exception_General(
          "Option \"useful\" is useless without option \"recursive\".");
  }
  if (vm.count("explicit")) {
    if (options->GetOnly() != consider_all)
      throw Exception_ExclusiveOptions("marked", "explicit");
    options->SetOnly(consider_explicit);
    options->SetExplicit(vm["explicit"].as<string>());
  }
  if (vm.count("marked")) {
    if (options->GetOnly() != consider_all)
      throw Exception_ExclusiveOptions("marked", "explicit");
    options->SetOnly(consider_marked);
    options->SetMarked(vm["marked"].as<string>());
  }
  if (vm.count("paths")) {
    options->SetPaths(vm["paths"].as< vector<string> >());
  }
  if (vm.count("recursive"))
    options->SetRecursive(true);

  return options;
}

OptionsGlobal* parse_profile(int argc, char* argv[]) {
  // Parsing when subcommand status is given.
  /*
   * Handling subcommand
   */
  set<pair<string, profile_subcommand> > all_subcommands;
  { // Initialisation of all_subcommands
    all_subcommands.insert(make_pair("copy", subcommand_copy));
    all_subcommands.insert(make_pair("new", subcommand_new));
    all_subcommands.insert(make_pair("delete", subcommand_delete));
    all_subcommands.insert(make_pair("list", subcommand_list));
  }
  profile_subcommand sub;

  if (argc <= 1) {
    sub = subcommand_error;
  } else {
    try {
      sub = findCommand<profile_subcommand>(
          argv[1], all_subcommands, subcommand_error);
    } catch (Exception_SeveralSubcommands& e) {
      print_error << "srsync: " << e.what() << endl;
      return NULL;
    }
    if (sub == subcommand_error) {
      ;
    } else {
      argc --;
      argv ++;
    }
  }

  /*
   * Parsing
   */
  OptionsGlobal* options = NULL;
  options_description visible("General options");
  visible.add_options()
    ("help,h", "Print help and exit.")
    ;
  options_description all;
  all.add_options()
    ("names", value<vector<string> >())
    ;
  all.add(visible);
  positional_options_description names;
  switch(sub) {
    case subcommand_new:
    case subcommand_delete:
      names.add("names", 1);
      break;
    case subcommand_copy:
      names.add("names", 2);
      break;
    case subcommand_list:
      names.add("names", 0);
      break;
    case subcommand_error:
      break;
  }

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).
        options(all).positional(names).run(), vm);
    notify(vm);
  }
  catch (boost::program_options::too_many_positional_options_error &e) {
    switch(sub) {
      case subcommand_new:
        print_error << "srsync-profile-new: Please provide a single argument: "
          "the profile to create." << endl;
        break;
      case subcommand_delete:
        print_error << "srsync-profile-delete: Please provide a single "
          "argument: the profile to delete." << endl;
        break;
      case subcommand_copy:
        print_error << "srsync-profile-copy: Please provide two arguments: "
          "source profile, and new profile." << endl;
        break;
      case subcommand_list:
        print_error << "srsync-profile-list: This subcommand does not accept "
          "any argument." << endl;
        break;
      case subcommand_error:
        print_error << "srsync-profile: please provide one of the following "
          "arguments to subcommand \"profile\": new, list, delete, copy."
          << endl;
        break;
    }
    return NULL;
  }

  /*
   * Analysing result, and executing commands.
   */
  if (vm.count("help")) {
    print_standard()
      << "Usage: srsync profile [--help] ACTION ARGUMENTS" << endl
      << "Manage profiles." << endl
      << endl
      << visible
      << endl
      << "The following actions are available:" << endl
      << "  list         \tList all available profiles" << endl
      << "  new NAME     \tCreate new empty profile NAME." << endl
      << "  copy SRC DEST\tCopy existing profile SRC to new profile DEST."
      << endl
      << "  delete NAME  \tDelete existing profile NAME." << endl
      ;
    options = new OptionsHelpVersion();
    return options;
  }

  vector<string> name_list;
  if (vm.count("names"))
    name_list = vm["names"].as<vector<string> >();
  switch(sub) {
    case subcommand_new:
      options = new OptionsNew(name_list[0]);
      break;
    case subcommand_delete:
      options = new OptionsDelete(name_list[0]);
      break;
    case subcommand_copy:
      options = new OptionsCopy(name_list[0], name_list[1]);
      break;
    case subcommand_list:
      options = new OptionsList();
      break;
    case subcommand_error:
      print_error << "srsync-profile: No action given. What do you want me to "
        "do?" << endl;
      options = NULL;
      break;
  }

  return options;
}

OptionsGlobal* parse_mark(int argc, char* argv[]) {
  // Parsing when subcommand mark is given.
  /*
   * Handling subcommand
   */
  set<pair<string, action> > all_actions;
  { // Initialisation of all_actions
    all_actions.insert(make_pair("accept", a_accept));
    all_actions.insert(make_pair("ask", a_ask));
    all_actions.insert(make_pair("descend", a_descend));
    all_actions.insert(make_pair("forget", a_forget));
    all_actions.insert(make_pair("reject", a_reject));
  }
  action act;

  if (argc <= 1) {
    act = a_none;
  } else {
    try {
      act = findCommand<action>(
          argv[1], all_actions, a_none);
    } catch (Exception_SeveralSubcommands& e) {
      print_error << "srsync: " << e.what() << endl;
      return NULL;
    }
    if (act == a_none) {
      ;
    } else {
      argc --;
      argv ++;
    }
  }

  /*
   * Parsing
   */
  OptionsGlobal* options = NULL;
  options_description hidden("hidden options");
  hidden.add_options()
    ("paths", value<vector<string> >())
    ;
  positional_options_description paths;
  paths.add("paths", -1);

  options_description general = options_update();
  general.add_options()
    ("force,f",  "do not ask before overwriting an existing configuration, "
     "and accept setting the status of non-existing paths.")
    ("interactive,i",  "ask before overwriting an existing configuration "
      "(default).")
    ("explicit,e", value<string>()->implicit_value("ynad"), "only consider "
      "files that are explicitly marked. If an optional argument is given, "
      "only consider files that are marked as specified by this argument: "
      "respectively y, n, d, a, x for paths marked as accepted, rejected, "
      "descend, ask, not marked.\nCombinations are allowed, e.g. \"-e yx\" "
      "will only consider paths that are not marked, or marked as accepted.")
    ("marked,m", value<string>()->implicit_value("ynad"), "only consider "
      "files that are explicitly or implicitely marked. If an optional "
      "argument is given, only consider files that are marked as specified "
      "by this argument: respectively y, n, d, a, x for paths marked as "
      "accepted, rejected, descend, ask, not marked.\nCombinations are "
      "allowed, e.g. \"-m yx\" will only consider paths that are not marked, "
      "or marked as accepted.")
    ("recursive,r",  "recursively perform modes on paths.")
    ;

  options_description all("");
  options_description visible("");
  visible.add(general);
  all.add(general).add(hidden);

  variables_map vm;
  store(command_line_parser(argc, argv).
      options(all).positional(paths).run(), vm);
  notify(vm);

  /*
   * Analysing result, and executing commands.
   */
  if (vm.count("help")) {
    print_standard()
      << "Usage: srsync mark [--help] ACTION PATHS" << endl
      << "Set the status of paths given in argument." << endl
      << endl
      << "The following actions are available:" << endl
      << "  accept \tMark the paths for being accepted." << endl
      << "  ask    \tMark the paths for action 'ask'." << endl
      << "  descend\tMark the paths for action 'descend'." << endl
      << "  forget \tForget configuration of path." << endl
      << "  reject \tMark the paths for not being accepted." << endl
      << endl
      << visible
      ;
    options = new OptionsHelpVersion();
    return options;
  }

  if (act == a_none) {
      print_error << "srsync-mark: No action given. What do you want me to do?"
        << endl;
      return NULL;
  }

  options = new OptionsMark(act);
  process_options_update(vm, options);

  if (vm.count("explicit")) {
    if (options->GetOnly() != consider_all)
      throw Exception_ExclusiveOptions("marked", "explicit");
    options->SetOnly(consider_explicit);
    options->SetExplicit(vm["explicit"].as<string>());
  }
  if (vm.count("marked")) {
    if (options->GetOnly() != consider_all)
      throw Exception_ExclusiveOptions("marked", "explicit");
    options->SetOnly(consider_marked);
    options->SetMarked(vm["marked"].as<string>());
  }
  if (vm.count("force")) {
    options->SetForce();
  }
  if (vm.count("interactive")) {
    options->SetInteractive();
  }

  if (vm.count("paths"))
    options->SetPaths(vm["paths"].as<vector<string> >());

  if (vm.count("recursive"))
    options->SetRecursive(true);

  return options;
}


// Parsing exec (and related, as rsync) subcommand
/*
 *** Subcommands "exec" and "rsync"
 * We want that options after '--options' are not analysed, but stored
 * somewhere, regardless of their meaning. To do so, we need to hack the
 * boost::program_options way to work.
 * We create two global variables:
 * - a vector of string 'remaining', which will store these extra arguments;
 * - a boolen 'CALL_OPTIONS', which can have the following values.
 *   - false if parsing is done by boost;
 *   - true if we store the options in 'remaining' before boost sees them.
 * We also create a fake option 'call_options' so that boost does not complain
 * that it did not get the option it gave us to parse: each option that we want
 * to hide from it is said to be a 'call_options' one.
 *
 * Then, we create a custom parse function 'call_options' which analyse
 * arguments before boost sees them.
 * - If we normally parse (CALL_OPTIONS == true), this function does nothing
 *   (i.e. return make_pair(string(), string());
 * - If we see a "--options" option, parsing of this option and its arguments
 *   goes normally, and we set parsing_state to CALL_OPTIONS == true.
 * - If we are collecting rsync options (CALL_OPTIONS = true), we
 *   store the option to 'remaining', and we return
 *   'make_pair(string("call_options"), string(" "))' to tell boost that we
 *   recognized and parsed a 'call_options'.
 *
 * Note: we are wasting time and space here. As 'call_options' is defined as
 * an option taking several string arguments, for each argument following
 * '--options', we store a space as the argument for option
 * 'call_options'. I think we could say boost that 'call_options' does not
 * take any argument, but I did not manage to do this.
 */
bool CALL_OPTIONS = false;
vector<string> remaining;
pair<string, string> call_options(const string& s)
{
  if (CALL_OPTIONS) {
      remaining.push_back(s);
      return make_pair(string("call_options"), string(" "));
  } else {
      if (s == "--options")
        CALL_OPTIONS = true;
      return make_pair(string(), string());
  }
  // Useless but lacks of it generates compiler warnings
  return make_pair(string(), string());
}
OptionsGlobal* parse_exec(int argc, char* argv[]) {
  // Parsing when subcommand exec or rsync is given.
  /*
   * Handling subcommand
   */
  set<pair<string, type_exec> > all_actions;
  { // Initialisation of all_actions
    all_actions.insert(make_pair("rsync", type_exec_rsync));
    all_actions.insert(make_pair("print", type_exec_print));
  }
  type_exec act;

  if (argc <= 1) {
    act = type_exec_rsync;
  } else if (string("rsync").find(argv[0]) == 0) {
    act = type_exec_rsync;
  } else {
    try {
      act = findCommand<type_exec>(
          argv[1], all_actions, type_exec_rsync);
    } catch (Exception_SeveralSubcommands& e) {
      print_error << "srsync: " << e.what() << endl;
      return NULL;
    }
    if (act == type_exec_rsync
        && string("rsync").find(argv[1]) == string::npos) {
      // Default is "rsync", but in this case, we do not remove the first item
      // of the options: it was not the subcommand
      ;
    } else {
      argc --;
      argv ++;
    }
  }

  /*
   * Parsing
   */
  OptionsGlobal* options = NULL;
  options_description hidden("hidden options");
  hidden.add_options()
    ("paths", value<vector<string> >())
    ("call_options", value<vector<string> >()->multitoken())
    ;
  positional_options_description paths;
  paths.add("paths", -1);

  options_description general = options_update();
  add_search_accepted(general);
  general.add_options()
    ("bin", value<string>(), "binary to use instead "
      "of the default one (most likely 'rsync') when performing "
      "synchronisation")
    ("careful,c", "print the command line to be executed and ask user to "
      "confirm before actually performing synchronisation")
    ("origin,o", value<string>(), "define an alternative origin base folder")
    ("options", "options to be passed to rsync.")
    ;


  options_description all("");
  options_description visible("");
  visible.add(general);
  all.add(general).add(hidden);

  variables_map vm;
  store(command_line_parser(argc, argv).
      options(all).extra_parser(call_options).positional(paths).run(), vm);
  notify(vm);

  /*
   * Analysing result, and executing commands.
   */
  if (vm.count("help")) {
    print_standard()
      << "Usage: srsync [exec ACTION|rsync] [--help] [SOURCE...] DEST "
        "[OPTIONS] [--options RSYNC_OPTIONS]" << endl
      << "Update configuration and synchronise files."
        << endl
      << endl
      << "Two subcommands are available:" << endl
      << "  exec ACTION\tSee below." << endl
      << "  rsync      \tShortcut for \"exec rsync\"." << endl
      << endl
      << "The following actions are available:" << endl
      << "  rsync\t\tUpdate configuration and synchronise files." << endl
      << "  print\t\tUpdate configuration and print the command that would "
        "have been executed with \"exec rsync\"." << endl
      << endl
      << "Arguments:" << endl
      << "Note: SOURCE, DEST and OPTIONS can be mixed, as long as DEST appears "
        "after SOURCE." << endl
      << "Note: --options must be the last option on the list. Content of "
        "RSYNC_OPTIONS is not analysed by srsync." << endl
      << "  SOURCE       \tpaths to synchronize with destination paths. They "
        "must be local files." << endl
      << "  DEST         \tdestination path. It can be anything supported by "
        "rsync." << endl
      << "  OPTIONS      \tsrsync options. See below." << endl
      << "  RSYNC_OPTIONS\trsync options. They are not processed and given "
        "directly to rsync." << endl
      << visible
      ;
    options = new OptionsHelpVersion();
    return options;
  }

  options = new OptionsExec(act);
  process_options_update(vm, options);
  process_options_search_accepted(vm, options);

  options->SetRsyncOptions(remaining);

  if (vm.count("bin")) {
    options->SetBin(vm["bin"].as<string>());
  }

  if (vm.count("origin")) {
    options->SetOrigin(vm["origin"].as<string>());
  }

  if (vm.count("careful")) {
    options->SetCareful(true);
  }
  vector<string> paths_list;
  if (vm.count("paths")) {
    paths_list = vm["paths"].as<vector<string> >();
    if (paths_list.size() < 2) {
      print_error << "srsync-exec: Give at least one source path and one "
        "destination file." << endl;
      delete options;
      return NULL;
    }
    options->SetDest(paths_list.back());
    paths_list.pop_back();
    options->SetPaths(paths_list);
    options->SetRsyncOptions(remaining);
  } else {
    print_error << "srsync-exec: Give at least one source path and one "
      "destination file." << endl;
    delete options;
    return NULL;
  }

  return options;
}

/*******************************************************************************
 * Parser
 ******************************************************************************/


OptionsGlobal* Parser::parse(int argc, char* argv[]) {
  /*
   * Parsing is done in the following steps:
   * - subcommands are handled
   * - options are defined
   * - parsing is performed
   * - result is analysed, and stored into 'options'
   */

  /*
   * OptionsGlobal object to be returned is called 'options'. It will be
   * defined as a subclass of OptionsGlobal according to the selected mode.
   */
  OptionsGlobal *options = NULL;

  try {
    /*
     * Handling subcommands
     */
    set<pair<string, subcommand> > all_subcommands;
    { // Initialisation of all_subcommands
      all_subcommands.insert(make_pair("exec", subcommand_exec));
      all_subcommands.insert(make_pair("rsync", subcommand_exec));
      all_subcommands.insert(make_pair("print", subcommand_print));
      all_subcommands.insert(make_pair("status", subcommand_status));
      all_subcommands.insert(make_pair("mark", subcommand_mark));
      all_subcommands.insert(make_pair("profile", subcommand_profile));
    }
    subcommand sub;

    if (argc <= 1) {
      sub = subcommand_none;
    } else {
      try {
        sub = findCommand<subcommand>
          (argv[1], all_subcommands, subcommand_none);
      } catch (Exception_SeveralSubcommands& e) {
        print_error << "srsync: " << e.what() << endl;
        return NULL;
      }
      if (sub == subcommand_none) {
        ;
      } else {
        argc --;
        argv ++;
      }
    }

    switch(sub) {
      case (subcommand_none):
        return parse_none(argc, argv);
      case (subcommand_print):
        return parse_print(argc, argv);
      case (subcommand_exec):
        return parse_exec(argc, argv);
      case (subcommand_status):
        return parse_status(argc, argv);
      case (subcommand_profile):
        return parse_profile(argc, argv);
      case (subcommand_mark):
        return parse_mark(argc, argv);
      case (subcommand_help):
      case (subcommand_version):
        // Should not happen, but g++ complains.
        return NULL;
    }
  }

  catch(boost::program_options::multiple_occurrences& e) {
    print_error << "srsync: "
      << "One of the option appears (at least) twice, whereas it should appear"
      << " at max once." << endl;
  }
  catch(boost::program_options::invalid_command_line_syntax& e) {
    print_error << "srsync: " << e.what() << endl;
  }
  catch(boost::program_options::unknown_option& e) {
    /*
     * Boost exception does not store the name of the wrong option alone, but
     * stores the string "unknown option OPTION". So, to get this option, we
     * have to extract it from this string, which explains the "substr".
     */
    print_error << "srsync: "
      << "Option '" << string(e.what()).substr(15) << "' does not exist."
      << endl;
  }
  catch(boost::program_options::invalid_option_value &e) {
    print_error << "srsync: "
      << "Invalid arguments for option '"
      << extractQuoted(1, '\'', e.what()) << "'." << endl;
  }
  catch(Options::Exception_General& e) {
    print_error << "srsync: " << e.what() << endl;
    if (options != NULL) {
      delete options;
      options = NULL;
    }
  }
  return options;
}
