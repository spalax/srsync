/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

/*******************************************************************************
 * This package parse the command line.
 ******************************************************************************/

#include<vector>
#include<set>

#include"global.h"
#include"options.h"
using namespace Options;

/*
 * Parse the command line.
 */
class Parser {
  public:
    /*
     * Arguments:
     *  'argc' and 'argv' are the same as those got as arguments of 'main'.
     * Return a subtype of option that contain the information got from the
     * command line.
     * Return NULL if some error occured.
     */
    OptionsGlobal* parse(int argc, char* argv[]);
};

/*
 * Extract the (n)th element of string 's' quoted by character 'c'. First
 * element has index 0.
 * Example: extractQuoted(1, '-', "A first -quoted element- and -a second
 * one-") returns "a second one".
 * Return an empty string if no quoted element was found, or if the string did
 * not contain enough quoted elements.
 */
string extractQuoted(int n, char c, string s);
