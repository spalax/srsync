/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

// Print to console
#include <iostream>
#include <sstream>
#include <stdexcept>

// Other packages from srsync
#include "io_std.h"
#include "options.h"
using namespace Options;

/*******************************************************************************
 * Options
 ******************************************************************************/
/*
 * Definition of a conversion to string, and of << for variables of type
 * 'subcommand'.
 */
string Options::subcommandToString(subcommand m) {
  switch(m) {
    case subcommand_help:
      return "help";
    case subcommand_version:
      return "version";
    case subcommand_mark:
      return "mark";
    case subcommand_print:
      return "print";
    case subcommand_exec:
      return "exec";
    case subcommand_status:
      return "status";
    case subcommand_profile:
      return "profile";
    case subcommand_none:
      return "none";
  }
  return "none"; // Useless, but g++ complains
}
template <class charT, class Traits> basic_ostream<charT, Traits> &operator<<
  (basic_ostream<charT, Traits> &stream, const subcommand &m) {
  typename basic_ostream<charT, Traits>::sentry init(stream);
  if (init)
    stream << subcommandToString(m);
  return stream;
}

string Options::profileToString(profile_subcommand p) {
  switch(p) {
    case subcommand_copy:
      return "copy";
    case subcommand_new:
      return "new";
    case subcommand_delete:
      return "delete";
    case subcommand_list:
      return "list";
    case subcommand_error:
      return "ERROR";
  }
  return "none"; // Useless, but g++ complains
}
template <class charT, class Traits> basic_ostream<charT, Traits> &operator<<
  (basic_ostream<charT, Traits> &stream, const profile_subcommand &m) {
  typename basic_ostream<charT, Traits>::sentry init(stream);
  if (init)
    stream << profileToString(m);
  return stream;
}

/*
 * Constructor
 */
OptionsGlobal::OptionsGlobal(){}

/*
 * Pure virtual destructor
 */
OptionsGlobal::~OptionsGlobal() {}

/*
 * Real mutators and accessors
 */
string OptionsGlobal::str_subcommand() {
  return subcommandToString(this->cl_subcommand);
}

/*
 * Dummy mutators and accessors
 */
void OptionsGlobal::SetAbsolute(bool) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "absolute");
}
void OptionsGlobal::SetExplicit(string) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "explicit");
}
void OptionsGlobal::SetMarked(string) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "marked");
}
void OptionsGlobal::SetExplicit(set<action>) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "explicit");
}
void OptionsGlobal::SetMarked(set<action>) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "marked");
}
void OptionsGlobal::SetOnly(consider) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "only");
}
void OptionsGlobal::SetForce() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "force");
}
void OptionsGlobal::SetInteractive() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "interactive");
}
void OptionsGlobal::SetNoAsk(action) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "no-ask");
}
void OptionsGlobal::SetOneShot(bool) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "one-shot");
}
void OptionsGlobal::SetQuiet(bool) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "quiet");
}
void OptionsGlobal::SetRecursive(bool){
  throw Exception_WrongSubcommand(this->str_subcommand(), "recursive");
}
void OptionsGlobal::SetUseful(bool){
  throw Exception_WrongSubcommand(this->str_subcommand(), "useful");
}
void OptionsGlobal::SetBin(string){
  throw Exception_WrongSubcommand(this->str_subcommand(), "bin");
}
void OptionsGlobal::SetCareful(bool){
  throw Exception_WrongSubcommand(this->str_subcommand(), "careful");
}
void OptionsGlobal::SetOrigin(string){
  throw Exception_WrongSubcommand(this->str_subcommand(), "origin");
}
void OptionsGlobal::SetDest(string){
  throw Exception_WrongSubcommand(this->str_subcommand(), "dest");
}
void OptionsGlobal::SetRsyncOptions(vector<string>){
  throw Exception_WrongSubcommand(this->str_subcommand(), "rsync-options");
}
void OptionsGlobal::SetPaths(vector<string>) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "paths");
}
void OptionsGlobal::SetPurge(bool){
  throw Exception_WrongSubcommand(this->str_subcommand(), "purge");
}
void OptionsGlobal::SetMark(action){
  throw Exception_WrongSubcommand(this->str_subcommand(), "mark");
}
void OptionsGlobal::SetProfile(string) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "profile");
}
void OptionsGlobal::SetShellOutput(string) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "shell-output");
}


consider OptionsGlobal::GetOnly() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "only");
}
action OptionsGlobal::GetMark() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "mark");
}
bool OptionsGlobal::GetRecursive() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "recursive");
}
bool OptionsGlobal::GetUseful() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "useful");
}
bool OptionsGlobal::GetForce() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "force");
}
string OptionsGlobal::GetDestProfile() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "dest");
}
vector<string> OptionsGlobal::GetPaths() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "paths");
}
bool OptionsGlobal::IsConsidered(action, action) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "considered");
}
bool OptionsGlobal::IsConsidered(action, bool) {
  throw Exception_WrongSubcommand(this->str_subcommand(), "considered");
}
set<action> OptionsGlobal::GetExplicit() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "explicit");
}
set<action> OptionsGlobal::GetMarked() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "marked");
}
bool OptionsGlobal::GetAbsolute() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "absolute");
}
bool OptionsGlobal::GetQuiet() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "quiet");
}
type_print OptionsGlobal::GetOutput() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "print");
}
bool OptionsGlobal::GetOneShot() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "one-shot");
}
action OptionsGlobal::GetNoAsk() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "no-ask");
}
vector<string> OptionsGlobal::GetRsyncOptions(){
  throw Exception_WrongSubcommand(this->str_subcommand(), "rsync-options");
}
string OptionsGlobal::GetDest(){
  throw Exception_WrongSubcommand(this->str_subcommand(), "dest");
}
string OptionsGlobal::GetBin(){
  throw Exception_WrongSubcommand(this->str_subcommand(), "bin");
}
bool OptionsGlobal::GetCareful(){
  throw Exception_WrongSubcommand(this->str_subcommand(), "careful");
}
type_exec OptionsGlobal::GetCommand(){
  throw Exception_WrongSubcommand(this->str_subcommand(), "exec");
}
string OptionsGlobal::GetOrigin(){
  throw Exception_WrongSubcommand(this->str_subcommand(), "origin");
}
bool OptionsGlobal::GetPurge(){
  throw Exception_WrongSubcommand(this->str_subcommand(), "purge");
}
profile_subcommand OptionsGlobal::GetProfileOperation() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "profile_operation");
}
string OptionsGlobal::GetProfile() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "profile");
}
string OptionsGlobal::GetSrcProfile() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "source_profile");
}
string OptionsGlobal::GetDstProfile() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "dest_profile");
}
string OptionsGlobal::GetShellOutput() {
  throw Exception_WrongSubcommand(this->str_subcommand(), "shell-output");
}

/*******************************************************************************
 * OptionsHelpVersion
 ******************************************************************************/
/*
 * Constructor of OptionsHelpVersion
 */
OptionsHelpVersion::OptionsHelpVersion():
  OptionsGlobal()
  { }

/*******************************************************************************
 * OptionsSearch
 ******************************************************************************/
/*
 * Constructor of OptionsSearch
 */
OptionsSearch::OptionsSearch():
  OptionsGlobal(),
  recursive(false),
  absolute(false),
  quiet(false),
  purge(false),
  only(consider_all)
  { }

// Mutators
void OptionsSearch::SetAbsolute(bool a) {
  this->absolute = a;
}

void OptionsSearch::SetPaths(vector<string> p) {
  this->paths = p;
}

void OptionsSearch::SetOnly(consider c) {
  this->only = c;
}

void OptionsSearch::SetQuiet(bool q) {
  this->quiet = q;
}

void OptionsSearch::SetRecursive(bool r) {
  this->recursive = r;
}

void OptionsSearch::SetPurge(bool p) {
  this->purge = p;
}
void OptionsSearch::SetProfile(string name) {
  this->profile = name;
}


/*
 * Accessors
 */
consider OptionsSearch::GetOnly() {
  return this->only;
}

vector<string> OptionsSearch::GetPaths() {
  return this->paths;
}

bool OptionsSearch::GetAbsolute() {
  return this->absolute;
}

bool OptionsSearch::GetQuiet() {
  return this->quiet;
}

bool OptionsSearch::GetRecursive() {
  return this->recursive;
}

bool OptionsSearch::GetPurge() {
  return this->purge;
}

bool OptionsSearch::IsConsidered(action expl, action impl) {
  switch(this->only) {
    case consider_all:
      return true;
    case consider_explicit:
      return this->explicitly_marked.count(expl) > 0;
    case consider_marked:
      return this->marked.count(impl) > 0;
  }
  return true; // Useless, but compiler complains.
}

bool OptionsSearch::IsConsidered(action act, bool inherited) {
  switch(this->only) {
    case consider_all:
      return true;
    case consider_explicit:
      return (!inherited) && (this->explicitly_marked.count(act) > 0);
    case consider_marked:
      return this->marked.count(act) > 0;
  }
  return true; // Useless, but compiler complains.
}
string OptionsSearch::GetProfile() {
  return this->profile;
}

/*******************************************************************************
 * OptionsMark
 ******************************************************************************/
/*
 * Constructor of OptionsMark
 */
OptionsMark::OptionsMark(action a):
  OptionsSearch(),
  mark(a),
  force(false)
{
  cl_subcommand = subcommand_mark;
}

void OptionsMark::SetForce() {
  this->force = true;
}
void OptionsMark::SetInteractive() {
  this->force = false;
}
void OptionsMark::SetExplicit(string arg) {
  string::iterator it;
  for ( it=arg.begin() ; it < arg.end(); it++ ) {
    switch(*it) {
      case 'a':
        this->explicitly_marked.insert(a_ask);
        break;
      case 'y':
        this->explicitly_marked.insert(a_accept);
        break;
      case 'n':
        this->explicitly_marked.insert(a_reject);
        break;
      case 'd':
        this->explicitly_marked.insert(a_descend);
        break;
      case 'x':
        this->explicitly_marked.insert(a_none);
        break;
      default:
        vector<string> accepted;
        string list[] = {"a", "y", "n", "d", "x"};
        accepted.insert(accepted.begin(), list, list + 5);
        throw Exception_WrongSet("explicit", arg, accepted, true);
    }
  }
}

void OptionsMark::SetMarked(string arg) {
  string::iterator it;
  for ( it=arg.begin() ; it < arg.end(); it++ ) {
    switch(*it) {
      case 'a':
        this->marked.insert(a_ask);
        break;
      case 'y':
        this->marked.insert(a_accept);
        break;
      case 'n':
        this->marked.insert(a_reject);
        break;
      case 'd':
        this->marked.insert(a_descend);
        break;
      case 'x':
        this->marked.insert(a_none);
        break;
      default:
        vector<string> accepted;
        string list[] = {"a", "y", "n", "d", "x"};
        accepted.insert(accepted.begin(), list, list + 5);
        throw Exception_WrongSet("marked", arg, accepted, true);
    }
  }
}

void OptionsMark::SetExplicit(set<action> a) {
  this->explicitly_marked = a;
}
void OptionsMark::SetMarked(set<action> a) {
  this->marked = a;
}
void OptionsMark::SetMark(action a) {
  this->mark = a;
}

action OptionsMark::GetMark() {
  return this->mark;
}
bool OptionsMark::GetForce() {
  return this->force;
}
set<action> OptionsMark::GetExplicit() {
  return this->explicitly_marked;
}
set<action> OptionsMark::GetMarked() {
  return this->marked;
}

/*******************************************************************************
 * OptionsSearchAccepted
 ******************************************************************************/
/*
 * Constructor of OptionsSearchAccepted
 */
OptionsSearchAccepted::OptionsSearchAccepted():
  OptionsSearch(),
  one_shot(false),
  no_ask(a_none),
  shell_output("default")
  { }

/*
 * Mutators
 */
void OptionsSearchAccepted::SetOneShot(bool o) {
  this->one_shot = o;
}
void OptionsSearchAccepted::SetNoAsk(action a) {
  this->no_ask = a;
}
void OptionsSearchAccepted::SetShellOutput(string o) {
  this->shell_output = o;
}

/*
 * Accessors
 */
bool OptionsSearchAccepted::GetOneShot() {
  return this->one_shot;
}
action OptionsSearchAccepted::GetNoAsk() {
  return this->no_ask;
}
string OptionsSearchAccepted::GetShellOutput() {
  return this->shell_output;
}

/*******************************************************************************
 * OptionsPrint
 ******************************************************************************/
/*
 * Constructor of OptionsPrint
 */
OptionsPrint::OptionsPrint(type_print a):
  OptionsSearchAccepted(),
  output(a)
{
  this->cl_subcommand = subcommand_print;
}

// Accessors
type_print OptionsPrint::GetOutput() {
  return this->output;
}

/*******************************************************************************
 * OptionsExec
 ******************************************************************************/
/*
 * Constructor of OptionsExec
 */
OptionsExec::OptionsExec(type_exec a):
  OptionsSearchAccepted(),
  dest(""),
  rsyncOptions(vector<string>()),
  bin("rsync"),
  careful(false),
  origin(""),
  command(a)
{
  this->cl_subcommand = subcommand_exec;
}

// Mutators
void OptionsExec::SetBin(string b) {
  this->bin = b;
}
void OptionsExec::SetCareful(bool c) {
  this->careful = c;
}
void OptionsExec::SetOrigin(string o) {
  this->origin = o;
}
void OptionsExec::SetDest(string d) {
  this->dest = d;
}
void OptionsExec::SetRsyncOptions(vector<string> ro) {
  this->rsyncOptions = ro;
}

// Accessors
vector<string> OptionsExec::GetRsyncOptions(){
  return this->rsyncOptions;
}
string OptionsExec::GetDest(){
  return this->dest;
}
string OptionsExec::GetBin(){
  return this->bin;
}
bool OptionsExec::GetCareful(){
  return this->careful;
}
type_exec OptionsExec::GetCommand(){
  return this->command;
}
string OptionsExec::GetOrigin(){
  return this->origin;
}

/*******************************************************************************
 * OptionsStatus
 ******************************************************************************/
/*
 * Constructor of OptionsStatus
 */
OptionsStatus::OptionsStatus():
  OptionsSearch(),
  useful(false)
{
  this->cl_subcommand = subcommand_status;
}

/*
 * Mutators
 */
void OptionsStatus::SetUseful(bool u){
  this->useful = u;
}
void OptionsStatus::SetExplicit(string arg) {
  string::iterator it;
  for ( it=arg.begin() ; it < arg.end(); it++ ) {
    switch(*it) {
      case 'a':
        this->explicitly_marked.insert(a_ask);
        break;
      case 'y':
        this->explicitly_marked.insert(a_accept);
        break;
      case 'n':
        this->explicitly_marked.insert(a_reject);
        break;
      case 'd':
        this->explicitly_marked.insert(a_descend);
        break;
      case 'x':
        this->explicitly_marked.insert(a_none);
        break;
      default:
        vector<string> accepted;
        string list[] = {"a", "y", "n", "d", "x"};
        accepted.insert(accepted.begin(), list, list + 5);
        throw Exception_WrongSet("explicit", arg, accepted, true);
    }
  }
}

void OptionsStatus::SetMarked(string arg) {
  string::iterator it;
  for ( it=arg.begin() ; it < arg.end(); it++ ) {
    switch(*it) {
      case 'a':
        this->marked.insert(a_ask);
        break;
      case 'y':
        this->marked.insert(a_accept);
        break;
      case 'n':
        this->marked.insert(a_reject);
        break;
      case 'd':
        this->marked.insert(a_descend);
        break;
      case 'x':
        this->marked.insert(a_none);
        break;
      default:
        vector<string> accepted;
        string list[] = {"a", "y", "n", "d", "x"};
        accepted.insert(accepted.begin(), list, list + 5);
        throw Exception_WrongSet("marked", arg, accepted, true);
    }
  }
}

void OptionsStatus::SetExplicit(set<action> a) {
  this->explicitly_marked = a;
}
void OptionsStatus::SetMarked(set<action> a) {
  this->marked = a;
}

/*
 * Accessors
 */
bool OptionsStatus::GetUseful() {
  return this->useful;
}
set<action> OptionsStatus::GetExplicit() {
  return this->explicitly_marked;
}
set<action> OptionsStatus::GetMarked() {
  return this->marked;
}


/*******************************************************************************
 * OptionsProfile
 ******************************************************************************/
/*
 * Constructor of OptionsProfile
 */
OptionsProfile::OptionsProfile():
  OptionsGlobal()
{ 
  this->cl_subcommand = subcommand_profile;
}

string OptionsProfile::str_subcommand(){
  ostringstream subcommand;
  subcommand << this->cl_subcommand << " " << this->profile_operation;
  return subcommand.str();
}

profile_subcommand OptionsProfile::GetProfileOperation() {
  return this->profile_operation;
}

/*******************************************************************************
 * OptionsCopy
 ******************************************************************************/
/*
 * Constructor of OptionsCopy
 */
OptionsCopy::OptionsCopy(string p1, string p2):
  OptionsProfile(),
  src(p1),
  dst(p2)
{
  this->profile_operation = subcommand_copy;
}

// Accessors
string OptionsCopy::GetSrcProfile(){
  return this->src;
}
string OptionsCopy::GetDstProfile(){
  return this->dst;
}

/*******************************************************************************
 * OptionsList
 ******************************************************************************/
/*
 * Constructor of OptionsList
 */
OptionsList::OptionsList():
  OptionsProfile()
{
  this->profile_operation = subcommand_list;
}

/*******************************************************************************
 * OptionsNew
 ******************************************************************************/
/*
 * Constructor of OptionsNew
 */
OptionsNew::OptionsNew(string profile):
  OptionsProfile(),
  profile(profile)
{
  this->profile_operation = subcommand_new;
}

// Accessor
string OptionsNew::GetProfile() {
  return this->profile;
}


/*******************************************************************************
 * OptionsDelete
 ******************************************************************************/
/*
 * Constructor of OptionsDelete
 */
OptionsDelete::OptionsDelete(string profile):
  OptionsProfile(),
  profile(profile)
{
  this->profile_operation = subcommand_delete;
}

// Accessor
string OptionsDelete::GetProfile() {
  return this->profile;
}

/*****************************************************************************
 * Exceptions related to options
 ****************************************************************************/
/*
 * Exception_General
 */
Exception_General::Exception_General(const string s):explanation(s) {
}

const string Exception_General::what() throw() {
  return this->explanation;
}

/*
 * Exception_WrongSubcommand
 */
Exception_WrongSubcommand::Exception_WrongSubcommand(string sub, string o):
  str_subcommand(sub),
  option(o){
    print_trace << "Wrong subcommand " << sub << " for option " << o << endl;
  }

const string Exception_WrongSubcommand::what() throw() {
  ostringstream cause;
  if (this->option == "paths")
    cause << "Positional arguments are";
  else
    cause << "Option '--" << this->option << "' is";
  cause << " irrelevant in subcommand '" << this->str_subcommand << "'.";
  return cause.str();
}

/*
 * Exception_WrongSet
 */
Exception_WrongSet::Exception_WrongSet(
    string o, string a, vector<string> s, bool c):
  option(o),
  arg(a),
  st(s),
  comb(c)
{}

const string Exception_WrongSet::what() throw() {
  ostringstream cause;
  cause << "Argument of option '--" << this->option << "' must be";
  if (this->comb)
    cause << " a combination of:";
  else
    cause << " one of:";
  // Printing set
  vector<string>::iterator it;
  vector<string> vs = this->st;
  for (it = vs.begin() ; it < vs.end() ; it++)
    cause << " " << *it;

  cause << " (now is '" << this->arg << "').\n";
  return cause.str();
}

/*
 * Exception_ExclusiveOptions
 */
Exception_ExclusiveOptions::Exception_ExclusiveOptions(string o1, string o2):
  option1(o1),
  option2(o2)
{}

const string Exception_ExclusiveOptions::what() throw() {
  ostringstream cause;
  cause << "Options '--" << this->option1 << "' and '--" << this->option2
    << "' are mutually exclusive.\n";
  return cause.str();
}
