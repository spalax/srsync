/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#include"action.h"

// Return the abbreviation corresponding to an action
char actionAbbr(action a) {
  switch(a) {
    case a_accept:
      return 'y';
    case a_ask:
      return 'a';
    case a_descend:
      return 'd';
    case a_forget:
      return 'x';
    case a_reject:
      return 'n';
    case a_none:
      return ' ';
  }
  return ' '; // Useless, but compiler complains
}
