/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

#include<string>

#include"global.h"

/*
 * This package handle operations concerning profile manipulation.
 */

/*
 * Create profile "default" if it does not exist.
 * Return "false" if an error occured.
 * Post condition: if this function returns true, profile "default" exists.
 */
bool createDefault();

/*
 * Copy a profile from 'src' to 'dst'.
 * This is simply a copy of file ~/.srsync/profile/src to file
 * ~/.srsync/profile/dst
 *
 * Return 'false' if an error occurs (trying to write on an existing profile,
 * cannot read source file, cannot write to file...
 */
bool profile_copy(string src, string dest);

/*
 * Create a new empty profile 'name'.
 * This is simply the creation of an empty file ~/.srsync/profiles/name
 *
 * Return 'false' if an error occured (file could not be written for instance).
 *
 * If this function returns 'true', this does NOT mean that profile was
 * deleted; only that no error occured: if argument is 'default', a prompt will
 * ask the user to confirm deletion. If the user refuse, the profile will not
 * be deleted, but no error will have occured, and 'true' will be returned.
 */
bool profile_new(string name);

/*
 * Delete the profile 'name'.
 * This is simply the deletion of file ~/.srsync/profiles/name
 *
 * Return 'false' if an error occured.
 */
bool profile_delete(string name);

/*
 * List all available profile.
 * This is a simple list of content of ~/.srsync/profiles
 */
bool profile_list();
