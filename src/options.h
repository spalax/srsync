/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

/*******************************************************************************
 * This file handle options of the program
 ******************************************************************************/

#include<vector>
#include<set>

#include"action.h"
#include"global.h"

namespace Options {
  /*
   * Commands must contain a subcommand (or have option --help or --version).
   * We create a virtual class Option, which stores the options set by the user,
   * and a subclass of Option for each of the subcommands.
   */

  /*
   * List of possible subcommands.
   */
  enum subcommand {
    subcommand_version, // Version asked (--version)
    subcommand_help, // Help asked (--help)
    subcommand_print, // Print files that would be synchronised (print)
    subcommand_exec, // Execute a command to synchronise files (exec or rsync)
    subcommand_status, // Print status of paths (status)
    subcommand_mark, // Mark paths (mark) (or forget information: forget)
    subcommand_profile, // Profile management. See type "enum
                        // profile_subcommand" for more details.
    subcommand_none // No subcommand defined
  };
  /*
   * List of possible actions on profile
   */
  enum profile_subcommand {
    subcommand_error, // No subcommand
    subcommand_copy, // Copy one profile to another (copy)
    subcommand_new, // Create a new empty profile (new)
    subcommand_delete, // Delete a profile (delete)
    subcommand_list // List all profiles (list)
  };

  /*
   * Definition of a conversion to string for variables of type 'subcommand'.
   */
  string subcommandToString(subcommand m);
  string profileToString(profile_subcommand p);

  /*
   * We gather classes having similar behaviors as subclasses of a common class.
   * The tree of classes is the following.
   * OptionsGlobal
   *    \_ OptionsHelpVersion (for subcommands help and version)
   *    \_ OptionsProfile
   *       \_ OptionsCopy
   *       \_ OptionsNew
   *       \_ OptionsDelete
   *       \_ OptionsList
   *    \_ OptionsSearch
   *       \_ OptionsMark
   *       \_ OptionsStatus
   *       \_ OptionsSearchAccepted
   *          \_ OptionsExec
   *          \_ OptionsPrint
   */

  enum consider {
    consider_all,
    consider_explicit,
    consider_marked
  };
  enum type_print {
    type_print_list,
    type_print_nlist,
    type_print_none
  };
  enum type_exec {
    type_exec_rsync,
    type_exec_print
  };

  /*
   * This class will store all the options that are set by the command line.
   */
  class OptionsGlobal {
    public:
      // Constructor
      OptionsGlobal();

      // Pure virtual destructor
      virtual ~OptionsGlobal() = 0;

      /*
       * Real mutators and accessors
       */
      virtual string str_subcommand();

      /*
       * The following mutators and accessors by default throw an exception
       * saying 'this member does not exist' when they are used. A subclass of
       * this class actually having this member will override them.
       */
      virtual void SetAbsolute(bool);
      virtual void SetExplicit(set<action>);
      virtual void SetMarked(set<action>);
      virtual void SetExplicit(string);
      virtual void SetMarked(string);
      virtual void SetOnly(consider);
      virtual void SetForce();
      virtual void SetInteractive();
      virtual void SetNoAsk(action);
      virtual void SetOneShot(bool);
      virtual void SetQuiet(bool);
      virtual void SetRecursive(bool);
      virtual void SetUseful(bool);
      virtual void SetBin(string);
      virtual void SetCareful(bool);
      virtual void SetOrigin(string);
      virtual void SetDest(string);
      virtual void SetRsyncOptions(vector<string>);
      virtual void SetPaths(vector<string>);
      virtual void SetPurge(bool);
      virtual void SetMark(action);
      virtual void SetProfile(string);
      virtual void SetShellOutput(string);

      virtual consider GetOnly();
      virtual action GetMark();
      virtual string GetDestProfile();
      virtual bool GetForce();
      virtual bool GetRecursive();
      virtual bool GetUseful();
      virtual vector<string> GetPaths();
      virtual set<action> GetExplicit();
      virtual set<action> GetMarked();
      virtual bool GetAbsolute();
      virtual bool GetQuiet();
      virtual bool IsConsidered(action, action);
      virtual bool IsConsidered(action, bool);
      virtual type_print GetOutput();
      virtual bool GetOneShot();
      virtual action GetNoAsk();
      virtual vector<string> GetRsyncOptions();
      virtual string GetDest();
      virtual string GetBin();
      virtual bool GetCareful();
      virtual type_exec GetCommand();
      virtual string GetOrigin();
      virtual bool GetPurge();
      virtual profile_subcommand GetProfileOperation();
      virtual string GetProfile();
      virtual string GetSrcProfile();
      virtual string GetDstProfile();
      virtual string GetShellOutput();

      // Subcommand
      subcommand cl_subcommand;
  };

  /*
   * Class containing all classes involving a search in the paths.
   */
  class OptionsSearch : public OptionsGlobal {
    friend class OptionsStatus;
    friend class OptionsMark;
    public:

      // Constructors
      OptionsSearch();

      // Mutators
      virtual void SetAbsolute(bool a);
      virtual void SetOnly(consider c);
      virtual void SetQuiet(bool q);
      virtual void SetPaths(vector<string> p);
      virtual void SetRecursive(bool);
      virtual void SetPurge(bool);
      virtual void SetProfile(string);

      // Accessor
      virtual consider GetOnly();
      virtual vector<string> GetPaths();
      virtual bool GetAbsolute();
      virtual bool GetQuiet();
      virtual bool GetRecursive();
      virtual bool GetPurge();
      virtual string GetProfile();

      /*
       * According to values of this.consider, this.explicitly_marked and
       * this.marked, return 'true' iff a path being explicitly marked
       * 'expl', and inheriting 'impl', is to be considered or not.
       */
      virtual bool IsConsidered(action expl, action impl);
      /*
       * Same function, excepted that the arguments are the action
       * (implicitely or explicitly) marked on path, and a bool to indicate
       * whether this action is implicit or not.
       */
      virtual bool IsConsidered(action act, bool inherited);

    private:
      // Options
      string profile; // Profile considered
      bool recursive; // --recursive
      bool absolute; // --absolute
      bool quiet; // --quiet
      vector<string> paths; // Paths to consider
      bool purge; // --purge
      // To store which of --explicit and --marked are used, we have an
      // enumerated type "consider", with values consider_all (none is used),
      // consider_explicit (consider explicitly marked), and consider_marked
      // (implicitely marked).
      // Then, the path to consider are those whose action is stored in
      // explicitly_marked (for --explicit) or marked (for --marked).
      consider only;
      set<action> explicitly_marked; // --explicit
      set<action> marked; // --marked
  };

  /*
   * Class containing all classes concerning profile manipulation.
   */
  class OptionsProfile : public OptionsGlobal {
    public:
      // Constructors
      OptionsProfile();

      // Accessors
      virtual profile_subcommand GetProfileOperation();

      // Representation for subcommand also takes the profile subcommand
      virtual string str_subcommand();

      // Which operation to do on profiles ?
      profile_subcommand profile_operation;
  };

  /*
   * Class containing all classes involving a search of accepted paths in the
   * paths.
   */
  class OptionsSearchAccepted : public OptionsSearch {
    public:
      // Constructors
      OptionsSearchAccepted();

      // Mutators
      virtual void SetOneShot(bool o);
      virtual void SetNoAsk(action a);
      virtual void SetShellOutput(string o);

      // Accessors
      virtual bool GetOneShot();
      virtual action GetNoAsk();
      virtual string GetShellOutput();

      // Options
      bool one_shot; // --one-shot
      action no_ask; // --no-ask
      string shell_output; //--shell-output
  };

  /*
   * Subclass of Options for subcommands consisting of marking some path. This
   * includes subcommands mark and forget.
   */
  class OptionsMark : public OptionsSearch {
    public:
      // Constructor
      OptionsMark(action a);

      // Mutators
      virtual void SetForce();
      virtual void SetInteractive();
      virtual void SetExplicit(set<action> a);
      virtual void SetMarked(set<action> a);
      virtual void SetExplicit(string);
      virtual void SetMarked(string);
      virtual void SetMark(action m);
      // Accessors
      virtual action GetMark();
      virtual bool GetForce();
      virtual set<action> GetExplicit();
      virtual set<action> GetMarked();

      // Considered mark
      action mark;

      // Options
      bool force; // --force
  };

  /*
   * Subclasss of Options for subcommand exec (and rsync).
   */
  class OptionsExec : public OptionsSearchAccepted {
    public:
      // Constructor
      OptionsExec(type_exec a);

      // Mutators
      virtual void SetBin(string b);
      virtual void SetCareful(bool c);
      virtual void SetOrigin(string o);
      virtual void SetDest(string d);
      virtual void SetRsyncOptions(vector<string>);

      // Accessors
      virtual vector<string> GetRsyncOptions();
      virtual string GetDest();
      virtual string GetOrigin();
      virtual string GetBin();
      virtual bool GetCareful();
      virtual type_exec GetCommand();

      // Options
      string dest; // DEST (first argument of rsync options)
      vector<string> rsyncOptions; // rsync options (everything that follows
                                   // --exec option)
      string bin; // --bin
      bool careful; // --careful
      string origin; // --origin
      const type_exec command; // --exec=COMMAND
  };

  /*
   * Subclass of Options for subcommand print.
   */
  class OptionsPrint : public OptionsSearchAccepted {
    public:
      // Constructor
      OptionsPrint(type_print a);

      // Accessors
      virtual type_print GetOutput();

      // Options
      const type_print output; // --print=OUTPUT
  };

  /*
   * Subclass of Options for subcommand status.
   */
  class OptionsStatus : public OptionsSearch {
    public:
      // Constructor
      OptionsStatus();

      // Mutators
      virtual void SetUseful(bool u);
      virtual void SetExplicit(set<action> a);
      virtual void SetMarked(set<action> a);
      virtual void SetExplicit(string);
      virtual void SetMarked(string);

      // Accessors
      virtual bool GetUseful();
      virtual set<action> GetExplicit();
      virtual set<action> GetMarked();

      // Options
      bool useful; // --useful
  };

  /*
   * Subclass of Options for --help or --version
   */
  class OptionsHelpVersion : public OptionsGlobal {
    public:
      // Constructor
      OptionsHelpVersion();
  };

  /*
   * Subclass of Options for --list
   */
  class OptionsList : public OptionsProfile {
    public:
      // Constructor
      OptionsList();
  };

  /*
   * Subclass of Options for --copy
   */
  class OptionsCopy : public OptionsProfile {
    public:
      // Constructor
      OptionsCopy(string p1, string p2);

      // Accessor
      virtual string GetDstProfile();
      virtual string GetSrcProfile();

      // Options
      const string src; // Source profile
      const string dst; // Destination profile
  };

  /*
   * Subclass of Options for --new
   */
  class OptionsNew : public OptionsProfile {
    public:
      // Constructor
      OptionsNew(string profile);

      // Accessor
      virtual string GetProfile();

    private:
      // Name of the profile to create
      const string profile;
  };

  /*
   * Subclass of Options for --delete
   */
  class OptionsDelete : public OptionsProfile {
    public:
      // Constructor
      OptionsDelete(string profile);

      // Accessor
      virtual string GetProfile();

    private:
      // Name of the profile to delete
      const string profile;
  };

  /*****************************************************************************
   * Exceptions related to options
   ****************************************************************************/

  /*
   * Base exception: all exceptions of this class inherit from this one.
   * Can be also used as a basic exception having only one string of
   * explanation, and nothing more.
   */
  class Exception_General : public std::exception {
    using std::exception::what;

    public:
      Exception_General() {};
      Exception_General(const string s);
      virtual ~Exception_General() throw() {};
      virtual const string what() throw();

    private:
      const string explanation;
  };

  /*
   * Exception thrown when we manipulate an option in the wrong subcommand.
   */
  class Exception_WrongSubcommand : public Exception_General {
    public:
      Exception_WrongSubcommand(string, string);
      ~Exception_WrongSubcommand() throw() {};
      virtual const string what() throw();

    private:
      string str_subcommand;
      string option;
  };

  /*
   * Exception thrown when the argument of an option (i.e. "--option=argument")
   * is outside the set it should belong to.
   */
  class Exception_WrongSet : public Exception_General {
    public:
      Exception_WrongSet(string o, string a, vector<string> s, bool c);
      ~Exception_WrongSet() throw() {};
      virtual const string what() throw();

    private:
      string option; // Option
      string arg; // Argument
      vector<string> st; // Accepted set
      bool comb; // Is 'true' if argument should be a combination of elements of
                 // the set; 'false' if it should be one of the elements.
  };

  /*
   * Exception that says that two options are mutually exclusive.
   */
  class Exception_ExclusiveOptions : public Exception_General {
    public:
      Exception_ExclusiveOptions(string o1, string o2);
      ~Exception_ExclusiveOptions() throw() {};
      virtual const string what() throw();

    private:
      string option1, option2;
  };

  /*****************************************************************************
   * Global variable: contain the options set by command line, and can be
   * accessed from anywhere in the code without passing it as an argument.
   ****************************************************************************/
  extern OptionsGlobal* global;
}
