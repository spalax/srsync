/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This package handle input and output with terminal.
 */

#include<iostream>
#include<string>
#define BOOST_FILESYSTEM_VERSION 3
#include<boost/filesystem.hpp>
#include<boost/filesystem/fstream.hpp>
namespace fs = boost::filesystem;

#include"global.h"
#include"action.h"

/*
 * Aliases for cout and cerr.
 */
// Traces (debug only)
extern std::ostream& print_trace;
// Errors (to stderr)
extern std::ostream& print_error;
// Null stream: stuff written here vanishes in deep space
extern std::ostream& print_null;
// Standard output
extern std::ostream& print_true_standard;

/* "Standard" print cannot always be standard output, for the following reason.
 * With option "--exec print", the rsync command is not executed but printed in
 * standard output. If we want to use this in a script, e.g. to store it and
 * use it a few lines before, it has to be the only thing printed in standard
 * output, otherwise:
 * - everything printed on screen will be stored, and a process work would be
 *   needed to extract the command to be ran;
 * - user would not see user feedback and prompt, since it would be stored in
 *   the script, as is done with the command.
 * To solve this, one has call set_standard_output(), once for all, to set
 * where to write stuff (default is "real" standard output), for now on. Then
 * to write something to standard output, one has to write to
 * print_standard(), which value depends upon value set by
 * set_standard_output().
 */
// Type of output where we can write to.
enum output_type {standard, error, null};
// Set the current output.
void set_standard_output(output_type);
// Get current output stream
extern std::ostream& print_standard();

/*
 * Some functions which interact with user (i.e. ask questions and expect
 * answers).
 */
namespace io_std {
  /*
   * Take a list of characters as argument, and prompt for one of them.
   * Argument:
   * - number: number of possible characters
   * - list: the list of characters (an array of 'number' char)
   * - def: default character
   *
   * Return:
   * the character chosen.
   * If end of stream is found, return character 'def'.
   *
   * Prompt until user return a valid answer.
   */
  char choose_char(int number, char list[], char def = '\0');

  /*
   * Ask confirmation for overwriting action 'o' of path 'p' with new action
   * 'n'.
   * Return true if overwriting is confirmed.
   */
  bool askOverwriteAction(fs::path p, action o, action n);

  /*
   * Ask confirmation about removing a path.
   */
  bool askConfirmRemove(fs::path);

  /*
   * Ask what to do with path 'p'. Current action of this path is 'current'.
   * Arguments:
   * - p: path to consider;
   * - current: current action for path "p";
   * - shell_output: file to redirect output of shell to (if option
   *   --shell-output is set). Accepted values are "none", "stdout", "stderr",
   *   and "default".
   * Precondition: 'current' is one of a_ask or a_none.
   */
  action askChooseAction(fs::path p, action current, string shell_output);

  /*
   * Ask user if he wants to execute the line that has been printed before.
   */
  bool askConfirmExecute();
}

/*
 * Handling null streams.
 */
class nullstreambuf : public std::streambuf {
protected:
   virtual int_type overflow(int_type c) 
   {
      return c;
   }
};
extern nullstreambuf globalnullstreambuf;
extern std::ostream globalnullstream;
