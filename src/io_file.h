/*
 * Copyright 2009-2010 Louis Paternault
 *
 *
 * This file is part of Srsync.
 * 
 * Srsync is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Srsync is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Srsync.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file handle reading from and writing to configuration files.
 */

#include"path.h"

/*
 * Perform some coherence checks over a path and its associated action.
 * Return true iff everything is coherent.
 *
 * - does the path exist?
 * - if its action is 'descend', is the path a directory?
 */
bool checkCoherent(fs::path p, action a);

/*
 * Return the Path structure corresponding to config file
 * "~/.srsync/profiles/profile".
 * Return NULL if some error occured.
 */
path::Path* readProfile(string profile);

/*
 * Write in profile "profile" information contained in object "root" (and its
 * descendants).
 * Return false if an error occurs.
 *
 * Profile is overwritten.
 * writeProfile(profile, readProfile(profile)) returns true and left profile
 * unchanged (up to line swappings).
 */
bool writeProfile(string profile, path::Path* root);
