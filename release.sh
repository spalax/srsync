#!/bin/bash
# Build tar.gz package
# Argument: destination directory

if [ "$#" != 1 ]
then
  echo "Usage: % DEST_DIRECTORY"
  echo "  Argument: directory in which to put the tar.gz archive when built."
  exit 1
fi
DEST="$1"

if [ ! -d "$DEST" ] || [ ! -r "$DEST" ]
then
  echo "Error: destination directory is not writable."
  exit 1
fi


echo "Which is the version of srsync (last seems to be $(head -n 1 ChangeLog | sed 's/.*(\(.*\)).*/\1/'))?"
read VERSION
DEBFULLNAME="Louis Paternault" DEBEMAIL="spalax@gresille.org" debchange --newversion $VERSION -m -c ChangeLog
echo "Change version."
bash


aclocal
autoconf
automake --add-missing
automake
./configure
make
make distcheck
mv srsync-*.tar.gz $DEST

echo "If everything went well:"
echo "# git ci"
echo "# git tag v\$VERSION"
echo "# git push"
echo "# git push --tags"
