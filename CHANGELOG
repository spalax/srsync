srsync (0.7.5) unstable; urgency=low

  * Changed project URL
  * Updated build process to take into account new version of dependencies

 -- Louis Paternault <spalax@gresille.org>  Sun, 27 Jan 2013 14:41:55 +0100

srsync (0.7.4) unstable; urgency=low

  * Corrected bug #006: Back to UNIX-like behaviour.
  * Corrected bug #007: When asked what to do with a file, printing its
    content is no longer printed on standard output with "srsync exec print
    ...".
  * Corrected bug #011: It is now possible to delete files when asked what to
    do with them.
  * Updated to work with both boost-1.42 and boost-1.46.

 -- Louis Paternault <spalax@gresille.org>  Sat, 13 Aug 2011 02:02:45 +0200

srsync (0.7.3) unstable; urgency=low

  * Corrected bug #010: it is now possible, in interactive mode, to stop
    asking action to perform on a file marked as "always ask".

 -- Louis Paternault <spalax@gresille.org>  Sun, 03 Apr 2011 14:31:31 +0200

srsync (0.7.2) unstable; urgency=low

  * Change to test commands, to correct a change introduced in new versions of
    atheist (test tool). Nothing changed in the program itself.

 -- Louis Paternault <spalax@gresille.org>  Fri, 18 Feb 2011 13:41:21 +0100

srsync (0.7.1) unstable; urgency=low

  * Corrected bug #008
  * Corrected bug #009

 -- Louis Paternault <spalax@gresille.org>  Sun, 19 Dec 2010 19:26:33 +0100

srsync (0.7.0) unstable; urgency=low

  * Corrected bug #006.
  * Corrected bug #007.
  * No longer fails if environment variable SHELL is not defined.
  * Minor code improvements.

 -- Louis Paternault <spalax@gresille.org>  Sun, 05 Dec 2010 17:58:55 +0100

srsync (0.6.3) unstable; urgency=low

  * Command line now uses subcommands instead of "modes", that is, commands
    beginning with "srsync profile" are used to handle profiles, "srsync mark"
    to (un)mark paths, "srsync exec" to update configuration and run rsync...
  * Corrected bug 'deleted': a problem appeared when a directory was deleted
    between the two passes of srsync, with subcommands exec or print.
  * Man page is now written in markdown, and generated with pandoc.
  * Some minor changes in release process.

 -- Louis Paternault <spalax@gresille.org>  Sun, 22 Aug 2010 16:00:57 +0200

srsync (0.6.2) unstable; urgency=low

  * Option "--exec print" is now useful.
  * Updated ./configure tests done before building
  * Updated help text
  * Minor improvements

 -- Louis Paternault <spalax@gresille.org>  Thu, 19 Aug 2010 19:38:44 +0200

srsync (0.6.1) unstable; urgency=low

  * Some bug corrections
  * Added option --purge

 -- Louis Paternault <spalax@gresille.org>  Tue, 27 Apr 2010 22:56:07 +0200

srsync (0.6.0) unstable; urgency=low

  * Use of GNU autoconf to build program
  * Added a test suite
  * Multiple source path were not handled well
  * Option --origin was not handlel well
  * Rsync option "--delete" is now relevant (was useless before)
  * Minor bug corrections

 -- Louis Paternault <spalax@gresille.org>  Sun, 11 Apr 2010 00:01:52 +0200

srsync (0.5.0) unstable; urgency=low

  * Some bug correction
  * Options --print and --exec work
  * Final version if no bug found (which is highly improbable), which means: I
    have implemented all that I thought about

 -- Louis Paternault <spalax@gresille.org>  Fri, 05 Feb 2010 01:10:13 +0200

srsync (0.4.0) unstable; urgency=low

  * Basic version of --print works. No options taken into account yet.

 -- Louis Paternault <spalax@gresille.org>  Fri, 29 Jan 2010 16:36:32 +0200

srsync (0.3.0) unstable; urgency=low

  * Following modes work:
    --accept
    --ask
    --descend
    --forget
    --reject
  * Minor improvements and bug fixes

 -- Louis Paternault <spalax@gresille.org>  Fri, 15 Jan 2010 17:28:06 +0200

srsync (0.1.0) unstable; urgency=low

  * Initial release. * Following modes work:
    --version
    --help
    --new
    --list
    --delete
    --copy
    --status

 -- Louis Paternault <spalax@gresille.org>  Mon, 11 Jan 2010 00:16:55 +0200
