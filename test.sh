#!/bin/bash

# Executing tests
FAILED=$(for testFile in $(find tests -name "*.test")
do
  pushd $(dirname $testFile) &>/dev/null
  echo -n "Testing $testFile... " > /dev/tty
  atheist $(basename $testFile) &>/dev/null
  if [ "$?" != 0 ]
  then
    printf " $testFile"
    echo "Failed" > /dev/tty
  else
    echo "Ok" > /dev/tty
  fi
  popd &>/dev/null
done)

# Printing summary
echo
if [ -z "$FAILED" ]
then
  echo "All tests succeeded."
else
  echo "The following tests failed."
  echo $FAILED
  exit 1
fi
