    This project is officially dead.
    
    I created it when I had a "small" backup hard drive, so I had to choose what to backup.
    Ten years later, I eventually bought a big backup hard drive, so I can backup everything.
    Thus, I no longer need srsync.

    -- Louis

I- Description
==============

Srsync  is  a tool to synchronize data between to directories. Its purpose is
to build, and possibly execute, a set of commands to be run  by rsync.

It  is  intended  to be a rsync with memory: user can tell once for all that he
want to save this and that directory, but to ignore this  other one,  and  the
program  will  remember  it.  Different actions permit a greater control over
synchronization.  See  section  ACTIONS  for  more information.

II- Licence
===========

Copyright 2010 - Louis Paternault

Srsync is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Srsync is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Srsync.  If not, see <http://www.gnu.org/licenses/>.

III - Install
=============

Installation is performed using the following commands::

    aclocal
    autoconf
    automake --add-missing
    ./configure
    make
    make install
